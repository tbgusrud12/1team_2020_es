package com.example.myongji_pay;

// MainActivity -> SignupActivity, LoginActivity [로그인 전의 화면]

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myongji_pay.DrawerMenu.Drawer_Language;
import com.example.myongji_pay.ResponseData.LoginResponse;
import com.example.myongji_pay.serivce.RestartService;

import org.ini4j.Ini;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;
import static com.example.myongji_pay.RetrofitClient.getApiService;

public class MainActivity extends AppCompatActivity {
    //메인 페이지에서 다음 페이지로 넘어갈 버튼과 화면 전환을 위한 Intent
    Button login_btn,signup_btn;
    String logined = "logined";
    Drawer_Language drawer_language = new Drawer_Language();
    public static Ini iniFile;
    private EditText id, pw;
    private RestartService restartService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        id = (EditText)findViewById(R.id.et_email);
        pw = (EditText)findViewById(R.id.et_password);
        login_btn  = (Button)findViewById(R.id.login);
        signup_btn = (Button)findViewById(R.id.signup);
        //초기 언어값을 설정해줌
        SharedPreferences cur_lan = getSharedPreferences("language", MODE_PRIVATE);
        Log.d("new_Tag",cur_lan.getString("language",""));

        //iniFile의 값을 얻어온다
        Load_Ini();
        //파일을 불러왔을 때 언어를 'kor'로 설정해준다.
        if(iniSection == null) {
            iniSection = iniFile.get("kor");
            //마지막 설정 값이 'eng'였으면 'eng'로 설정해준다
            if(cur_lan.getString("language","").equals("eng")){
                iniSection = iniFile.get("eng");
            }
        }
        //ini 파일 적용
        id.setHint(iniSection.get("id_hint"));
        pw.setHint(iniSection.get("pwd_hint"));
        login_btn.setText(iniSection.get("login"));
        signup_btn.setText(iniSection.get("signup"));
        SharedPreferences sf = getSharedPreferences("permission", MODE_PRIVATE);
        SharedPreferences.Editor editor = sf.edit();
//        editor.clear();
//        editor.commit();
        if(sf.getString("permission","").equals("allowed")){
            Intent intent = new Intent(getApplicationContext(), FingerPrinter.class);
            intent.putExtra("option", "login");
            startActivity(intent);
        }

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //테스트용. admin id만 입력하면 Mainactivity2페이지로 넘어감
                if(id.getText().toString().equals("admin")){

                    Intent intent = new Intent(getApplication(), MainActivity2.class);
                    startActivity(intent);
                }
                else loginRequest();
            }
        });

        signup_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplication(), SignupActivity.class);
                startActivity(intent);
            }
        });


    }


    public void loginRequest(){
        //JSONObject parameters = new JSONObject(params);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("userId", id.getText().toString());
        params.put("password", pw.getText().toString());

        //POST 요청
        Call<LoginResponse> call = getApiService(this).postLogIn(params);
        call.enqueue(new Callback<LoginResponse>(){
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if(response.isSuccessful()){
                    Log.d("postLogIn", "success POST");
                    User.id = response.body().getId();
                    User.userId = response.body().getUserId();
                    User.userName = response.body().getUsername();
                    User.access_token = response.body().getAccess_token();
                    Log.d("1", "1: " + User.userName);
                    Intent intent = new Intent(getApplication(), MainActivity2.class);
                    startActivity(intent);
                }
                else{
                    Log.e("postLogIn", "error code: " + response.code());

                   if(response.code() == 401)
                        printToast(iniSection.get("toast_enter_id"));
                   else if(response.code() == 402)
                        printToast(iniSection.get("toast_enter_pwd"));
                    else if(response.code() == 403) {
                       printToast(iniSection.get("toast_confirm_id"));
                       id.setText(null);
                       pw.setText(null);
                       id.requestFocus();
                       showKeypad();
                   }
                    else if(response.code() == 404) {
                       printToast(iniSection.get("toast_confirm_password"));
                       pw.setText(null);
                       pw.requestFocus();
                       showKeypad();
                   }
                }
            }
            @Override
            public void onFailure(@NonNull Call<LoginResponse> call, @NonNull Throwable t){
                Log.e("postLogIn", "에러 : " + t.getMessage());
            }
        });

    }
    public void printToast(String data){
        Toast myToast = Toast.makeText(this.getApplicationContext(), data, Toast.LENGTH_SHORT);
        myToast.show();
    }
    public Serializable Load_Ini() {
        AssetManager assetManager = getResources().getAssets();
        InputStream inputStream;
        try {
            inputStream = assetManager.open("ini/language.ini");
            Log.d("tag", "fileOpen");
            iniFile = new Ini(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return iniFile;
    }
    public void showKeypad(){
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }
}