package com.example.myongji_pay;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;

public class MyAccountAdapter extends RecyclerView.Adapter<MyAccountAdapter.ViewHolder> {
    private ArrayList<Account> mDataset;
    private LayoutInflater inflater;
    Context context;


    public String getFinNum(int position){
        return mDataset.get(position).getFintechUseNum();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public CardView cv;
        public TextView account_name;
        public TextView account_balance;

        public ViewHolder(View v) {
            super(v);
            cv = (CardView)v.findViewById(R.id.card_view);
            account_name = (TextView)v.findViewById(R.id.account_name);
            account_balance = (TextView)v.findViewById(R.id.account_balance);
        }
    }

    public MyAccountAdapter(Context context, ArrayList<Account> myDataset){
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.mDataset = myDataset;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.account_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.account_name.setText(mDataset.get(position).bank + " " + mDataset.get(position).getAccount_num());
        holder.account_balance.setText(String.valueOf(mDataset.get(position).getAccount_balance())+iniSection.get("won")); //int를 가져온다는점 유의
        holder.cv.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Bundle bundle = new Bundle(1);
                bundle.putString("fintech_use_num", getFinNum(position));
                ((MainActivity2)context).replaceFragment(Wire_Bank_Test.newInstance(), bundle);
            }
        });
    }
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void printToast(String data){
        Toast.makeText(context, data, Toast.LENGTH_SHORT).show();
    }

}
