package com.example.myongji_pay;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.myongji_pay.BottomMenu._MyCard;
import com.example.myongji_pay.ResponseData.CardResponse;
import com.github.gcacace.signaturepad.views.SignaturePad;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.myongji_pay.RetrofitClient.getApiService;
import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;
public class AddCardSign extends Fragment {
    Button clear_btn,btnNext;
    SignaturePad mSignaturePad;
    private boolean success = false;
    String bank,cardNumber_d,cardNumber,validity,cardCvc,cardPassword, cardColor;


    public static AddCardSign newInstance() {
        return new AddCardSign();
    }

    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.add_card_signature, container, false);

        //상단 툴바 설정
        Toolbar sub_toolbar = (Toolbar)view.findViewById(R.id.sub_toolbar);
        ((MainActivity2)getActivity()).setSupportActionBar(sub_toolbar);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        //사인 추가 페이지 ini파일 다국어 설정
        TextView add_card =(TextView)view.findViewById(R.id.secText);
        TextView add_sign =(TextView)view.findViewById(R.id.add_sign);
        btnNext = view.findViewById(R.id.btnNext);
        add_card.setText(iniSection.get("add_card"));
        add_sign.setText(iniSection.get("register_signature"));
        btnNext.setText(iniSection.get("next"));
        clear_btn = view.findViewById(R.id.clear_btn);
        mSignaturePad = view.findViewById(R.id.signature_pad);

        clear_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSignaturePad.clear();
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postAddCard();

            }
        });
        return view;
    }
    // 서버에 입력된 앱비밀번호 수정 request
    public void postAddCard(){
        //params에 입력데이터 삽입
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("bank", bank);
        params.put("cardNumber_d", cardNumber_d);
        params.put("cardNumber", cardNumber);
        params.put("cardColor", cardColor);
        params.put("validity",validity);
        params.put("cardCvc", cardCvc);
        params.put("cardPassword", cardPassword);

        Log.d("2", String.valueOf(params));

        // PATCH 요청
        Call<CardResponse> call = getApiService(getActivity()).postCard(params);
        call.enqueue(new Callback<CardResponse>() {
            @Override
            public void onResponse(Call<CardResponse> call, Response<CardResponse> response) {
                if(response.isSuccessful()){
                    Log.d("postCard", "success Post");
                    success = true;
                    //Card.id = response.body().getId();
                    printToast(iniSection.get("toast_card_regist"));
                    FragmentManager fm = getParentFragmentManager();
                    fm.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    ((MainActivity2)getActivity()).replaceFragment(_MyCard.newInstance(),null);
                }
                else{
                    Log.e("patch_pwd", "error code: " + response.code());
                    if(response.code() == 400)
                        printToast(iniSection.get("toast_not_entervalue"));
                    else if(response.code() == 500)
                        printToast(iniSection.get("toast_server_error"));
                }
            }
            @Override
            public void onFailure(@NonNull Call<CardResponse> call, @NonNull Throwable t){
                Log.e("patch_pwd", "에러 : " + t.getMessage());
            }
        });
    }
    public void printToast(String data){
        Toast.makeText(this.getActivity(), data, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStart() {
        super.onStart();
        if(getArguments() != null) {
            Bundle card = this.getArguments();
            bank = card.getString("bank");
            Log.d("1",bank);
            cardNumber_d= card.getString("cardNumber_d");
            cardNumber=card.getString("cardNumber");
            validity  =card.getString("validity");
            cardCvc =card.getString("cardCvc");
            cardPassword =card.getString("cardPassword");
            cardColor = card.getString("cardColor");

            Log.d("1",bank);
            Log.d("1",cardNumber_d);
            Log.d("1",cardNumber);
            Log.d("1",validity);
            Log.d("1",cardCvc);
            Log.d("1",cardPassword);
            Log.d("1",cardColor);
        }
    }
}



