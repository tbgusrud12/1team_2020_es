package com.example.myongji_pay;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.myongji_pay.DrawerMenu.Drawer_Membership_End;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;
import static com.example.myongji_pay.RetrofitClient.getApiService;

public class MyMembershipAdapter extends BaseAdapter {
    private boolean success =false;
    private Context context;
    private ArrayList<String> array_card;
    private ArrayList<String> array_cardNum;
    private ArrayList<String> array_cardColor;
    private ArrayList<String> array_cardId;
    private ListView listView;


    private ViewHolder viewHolder;

    public MyMembershipAdapter(Context context, ArrayList<String> array_card, ArrayList<String> array_cardNo, ArrayList<String> array_cardColor, ArrayList<String> array_cardId, ListView listView){
        this.context=context;
        this.array_card=array_card;
        this.array_cardNum=array_cardNo;
        this.array_cardColor=array_cardColor;
        this.array_cardId=array_cardId;
        this.listView = listView;
    }
    public Context getContext(){
        return context;
    }
    @Override
    public int getCount(){
        return array_card.size();
    }
    @Override
    public Object getItem(int position){
        return array_card.get(position);
    }

    public String getCardId(int position){
        return array_cardId.get(position);
    }
    public String getMembershipName(int position){
        return array_card.get(position);
    }
    public String getMembershipNum(int position){
        return array_cardNum.get(position);
    }
    @Override
    public long getItemId(int position){
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        if(convertView == null){
            convertView= LayoutInflater.from(context).inflate(R.layout.card_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }else{
            viewHolder=(ViewHolder)convertView.getTag();
        }
        viewHolder.card_com.setText(array_card.get(position));
        viewHolder.card_nick.setText(array_cardNum.get(position));
        if(array_cardColor.get(position).equals("navy"))
            viewHolder.card.setBackgroundResource(R.drawable.card_navy);
        else if(array_cardColor.get(position).equals("green"))
            viewHolder.card.setBackgroundResource(R.drawable.card_green);
        else if(array_cardColor.get(position).equals("red"))
            viewHolder.card.setBackgroundResource(R.drawable.card_red);
        else if(array_cardColor.get(position).equals("orange"))
            viewHolder.card.setBackgroundResource(R.drawable.card_orange);
        else if(array_cardColor.get(position).equals("yellow"))
            viewHolder.card.setBackgroundResource(R.drawable.card_yellow);
        else if(array_cardColor.get(position).equals("purple"))
            viewHolder.card.setBackgroundResource(R.drawable.card_purple);
        else if(array_cardColor.get(position).equals("black"))
            viewHolder.card.setBackgroundResource(R.drawable.card_black);

        viewHolder.card_com.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Bundle bundle = new Bundle(1);
                bundle.putString("Membership", getMembershipName(position));
                bundle.putString("MembershipNum", getMembershipNum(position));
                ((MainActivity2)getContext()).replaceFragment(Drawer_Membership_End.newInstance(), bundle);
            }
        });

        Button btn = (Button)convertView.findViewById(R.id.card_more_btn);
        btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                PopupMenu p = new PopupMenu(getContext(), v);
                p.getMenuInflater().inflate(R.menu.card_menu2, p.getMenu());
                p.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                       if(item.getItemId()==R.id.card_m2){
                           deleteMembership(position);
                        }

                        return true;
                    }
                });
                p.show();
            }
        });

        return convertView;
    }
    public void deleteMembership(int position){
        Call<Void> call = getApiService(getContext()).deleteMembership(getCardId(position));
        Log.d("call", call +"d");
        call.enqueue(new Callback<Void>(){
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.isSuccessful()){
                    success = true;
                    printToast(iniSection.get("toast_card_delet"));
        //            ((MainActivity2)getContext()).refreshFragment(Drawer_Membership.newInstance(),null);
                    array_cardId.remove(position);
                    array_card.remove(position);
                    array_cardColor.remove(position);
                    array_cardNum.remove(position);
                    ((MyMembershipAdapter)listView.getAdapter()).notifyDataSetChanged();
                }
                else{
                    if(response.code()==500)
                        printToast(iniSection.get("toast_server_error"));
                    success = false;
                }

            }
            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t){
                success = false;
                Log.e("deleteMembership", "에러 : " + t.getMessage());
            }
        });
    }
    public void printToast(String data){
        Toast.makeText(getContext(), data, Toast.LENGTH_SHORT).show();
    }

    public class ViewHolder{
        private TextView card_com;
        private TextView card_nick;
        private View card;

        public ViewHolder(View convertView){
            card_com=(TextView)convertView.findViewById(R.id.card_company);
            card_nick=(TextView)convertView.findViewById(R.id.card_nickname);
            card=(View)convertView.findViewById(convertView.getId());
        }
    }
}
