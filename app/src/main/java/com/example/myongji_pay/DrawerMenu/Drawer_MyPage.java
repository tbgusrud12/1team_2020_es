package com.example.myongji_pay.DrawerMenu;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.example.myongji_pay.MainActivity2;
import com.example.myongji_pay.R;
import com.example.myongji_pay.Signout;
import com.example.myongji_pay.User;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;
import static com.example.myongji_pay.RetrofitClient.getApiService;
public class Drawer_MyPage extends Fragment{
    private String pwd;
    private View view;
    private boolean success;
    public static Drawer_MyPage newInstance(){
        return new Drawer_MyPage();
    }
    TextView birth, id, phone_num1;
    String phone_num;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.drawer_mypage,container,false);
         Toolbar sub_toolbar = (Toolbar)view.findViewById(R.id.sub_toolbar);
        ((MainActivity2)getActivity()).setSupportActionBar(sub_toolbar);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        //내 정보 페이지 ini 파일 다국어 적용
        TextView my_info = (TextView)view.findViewById(R.id.secText);
        TextView username_info = (TextView)view.findViewById(R.id.username_info);
        TextView user_name = (TextView)view.findViewById(R.id.user_name);
        TextView edit = (TextView)view.findViewById(R.id.edit_btn);
        TextView sign_out = (TextView)view.findViewById(R.id.sign_out);
        TextView name = (TextView)view.findViewById(R.id.name);
        TextView ID = (TextView)view.findViewById(R.id.id_1);
        TextView phone_num_txt = (TextView)view.findViewById(R.id.phone_number);

        my_info.setText(iniSection.get("info"));
        username_info.setText(User.userName+ iniSection.get("user_info"));
        user_name.setText(User.userName);
        name.setText(iniSection.get("name_hint"));
        ID.setText(iniSection.get("id_hint"));
        phone_num_txt.setText(iniSection.get("phone_number"));
        edit.setText(iniSection.get("modify"));
        sign_out.setText(iniSection.get("withdrawl"));
        /*수정 버튼 클릭 시 비밀번호를 확인하고 수정화면으로 이동*/

        edit.setOnClickListener(new View.OnClickListener(){ //수정 버튼 클릭 시 AlertDialog 띄워 비밀번호 입력받음
            @Override
            public void onClick(View v) {
                final EditText editText = new EditText(getContext().getApplicationContext());
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(iniSection.get("toast_enter_pwd"));
                builder.setView(editText);
                editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                builder.setPositiveButton(iniSection.get("check"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        pwd = editText.getText().toString();
                        //테스트용
                        if(pwd.equals("")){
                            ((MainActivity2)getActivity()).replaceFragment(Drawer_MyPage_edit.newInstance(),null);
                        }
                        //
                        Log.d("d",pwd);
                        pwdCheckRequest();

                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });
        phone_num1 = view.findViewById(R.id.user_phone_number);
        if(phone_num != null) {
            phone_num1.setText(phone_num);
            Log.d("phone_num1", phone_num+"e");
        }
        id = view.findViewById(R.id.id);
        id.setText(User.userId);

        /*회원탈퇴 클릭 시 sign_out화면으로 이동*/

        sign_out.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(getActivity(), Signout.class);
                startActivity(intent);
            }
        });
        return view;
    }
    public void pwdCheckRequest(){
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("password", pwd);
        Call<String> call = getApiService(getActivity()).postpwdCheck(params);
        call.enqueue(new Callback<String>(){
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    Log.d("d","1");
                    success = true;
                    ((MainActivity2)getActivity()).replaceFragment(Drawer_MyPage_edit.newInstance(),null);
                }
                else{
                    Log.d("d","2");
                    success = false;
                    printToast(iniSection.get("toast_pwd_not_match"));

                }

            }
            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t){
                success = false;
                printToast(iniSection.get("toast_pwd_not_match"));
            }
        });
    }
    public void printToast(String data){
        Toast.makeText(this.getActivity(), data, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if(getArguments() != null) {
            Bundle edit = this.getArguments();
            phone_num = edit.getString("Phone_num");
            Log.d("phone_num", phone_num);
        }
    }
}
