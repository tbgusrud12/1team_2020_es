package com.example.myongji_pay.DrawerMenu;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.example.myongji_pay.MainActivity2;
import com.example.myongji_pay.R;
import com.example.myongji_pay.SetPassWd_Change;

import org.w3c.dom.Text;

import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;

public class Drawer_Auth_Security extends Fragment {
    private View view;
    public static Drawer_Auth_Security newInstance(){return new Drawer_Auth_Security();}
    Button btnPassWd,btnfinger;
    Switch fingerprint;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.drawer_auth_security, container, false);

        Toolbar sub_toolbar = (Toolbar)view.findViewById(R.id.sub_toolbar);
        ((MainActivity2)getActivity()).setSupportActionBar(sub_toolbar);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        //인증/보안 페이지 ini파일 다국어 설정
        TextView auth_Security = (TextView)view.findViewById(R.id.secText) ;
        btnPassWd = view.findViewById(R.id.btnPassWd);
        btnfinger = view.findViewById(R.id.btnfinger);
        auth_Security.setText(iniSection.get("security"));
        btnPassWd.setText(iniSection.get("pwd_set"));
        btnfinger.setText(iniSection.get("fingerprint_set"));
        fingerprint = view.findViewById(R.id.fingerprint);
        btnPassWd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity2)getActivity()).replaceFragment(SetPassWd_Change.newInstance(),null);
            }
        });

        fingerprint.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            SharedPreferences sf = getActivity().getSharedPreferences("permission", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sf.edit();
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked == true){
                    editor.putString("permission", "allowed");
                    Toast.makeText(getActivity(),iniSection.get("fingerprint_on"), Toast.LENGTH_SHORT).show();
                }else{
                    editor.putString("permission", "denied");
                    Toast.makeText(getActivity(),iniSection.get("fingerprint_off"), Toast.LENGTH_SHORT).show();
                }
                editor.commit();
            }
        });
        return view;
    }

}
