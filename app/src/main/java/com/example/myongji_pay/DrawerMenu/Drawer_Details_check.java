package com.example.myongji_pay.DrawerMenu;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myongji_pay.MainActivity2;
import com.example.myongji_pay.R;
import com.example.myongji_pay.ResponseData.TransactionAllResponse;
import com.example.myongji_pay.TransactionData;
import com.example.myongji_pay.TransactionDetailAdpater;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;
import static com.example.myongji_pay.RetrofitClient.getApiService;

public class Drawer_Details_check extends Fragment implements TransactionDetailAdpater.OnItemClickListener {
    private TransactionDetailAdpater adapter;
    private View view;
    public static Drawer_Details_check newInstance(){return new Drawer_Details_check();}
    RecyclerView mTimeRecyclerView;
    RecyclerView.LayoutManager layoutManager;
    private String fromDate, toDate;
    ArrayList<TransactionData> dataset;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.transaction_detail,container,false);
        //화면 띄울시 stack에 push
//        Fragment currentFragment = MainActivity2.fm.findFragmentById(R.id.main_frame);
//        MainActivity2.fragmentStack.push(currentFragment);
        //상단 툴바 설정
        Toolbar sub_toolbar = (Toolbar)view.findViewById(R.id.sub_toolbar);
        ((MainActivity2)getActivity()).setSupportActionBar(sub_toolbar);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        //통합내역조회 페이지 ini파일 다국어 설정
        TextView details = (TextView)view.findViewById(R.id.secText) ;
        details.setText(iniSection.get("details_check"));

        fromDate = "20201101";
        toDate = "20201124";

        dataset = new ArrayList<>();
        getTranListRequest();

        return view;
    }

    @Override
    public void onItemClick(int position) {
        Toast.makeText(getContext(), adapter.getItem(position).getTitle(), Toast.LENGTH_SHORT).show();
    }

    private void getTranListRequest() {
        Call<ArrayList<TransactionAllResponse>> call = getApiService(getActivity()).getTransactionAll(fromDate, toDate);
        call.enqueue(new Callback<ArrayList<TransactionAllResponse>>(){
            @Override
            public void onResponse(Call<ArrayList<TransactionAllResponse>> call, Response<ArrayList<TransactionAllResponse>> response) {
                if(response.isSuccessful()){
                    List<TransactionAllResponse> responseList = response.body();
                    for(TransactionAllResponse item : responseList){
                        Log.d("getTransactionAll","bank name : " + item.getBankName() + ", tran date : " + item.getTranDate() + ", tran time : " + item.getTranTime() + ", inout type : " + item.getInoutType() + ", content : " + item.getContent() + ", amount : " + item.getAmount());
                        dataset.add(new TransactionData(item.getContent(), item.getAmount() + "원", getTime(item.getTranTime()), item.getBankName(), item.getInoutType(), Integer.parseInt(item.getTranDate().substring(0,4)), Integer.parseInt(item.getTranDate().substring(4,6)), Integer.parseInt(item.getTranDate().substring(6,8))));
                    }
                    Log.d("getTransactionAll", "success GET");
                    showFrag();
                }
                else{
                    Log.e("getTransactionAll", "error code: " + response.code());
                    if(response.code() == 500)
                        printToast(iniSection.get("toast_server_error"));
                }
            }
            @Override
            public void onFailure(@NonNull Call<ArrayList<TransactionAllResponse>> call, @NonNull Throwable t){
                Log.e("getTransactionAll", "에러 : " + t.getMessage());
            }
        });
    }

    String getTime(String t){
        return t.substring(0,2) + ":" + t.substring(2,4);
    }
    public void printToast(String data){
        Toast.makeText(this.getContext(), data, Toast.LENGTH_SHORT).show();
    }

    public void showFrag(){
        mTimeRecyclerView = (RecyclerView)view.findViewById(R.id.transactions_list);
        mTimeRecyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getContext());
        mTimeRecyclerView.setLayoutManager(layoutManager);

        adapter = new TransactionDetailAdpater(dataset);
        adapter.setOnItemClickListener(this);
        mTimeRecyclerView.setAdapter(adapter);
    }

}
