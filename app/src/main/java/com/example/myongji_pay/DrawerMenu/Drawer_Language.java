package com.example.myongji_pay.DrawerMenu;


import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.example.myongji_pay.MainActivity2;
import com.example.myongji_pay.R;

import org.ini4j.Ini;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import static android.content.Context.MODE_PRIVATE;

public class Drawer_Language extends Fragment {
    private View view;
    public static Ini iniFile;
    public static Ini.Section iniSection;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.drawer_language, container, false);

        Toolbar sub_toolbar = (Toolbar) view.findViewById(R.id.sub_toolbar);
        ((MainActivity2) getActivity()).setSupportActionBar(sub_toolbar);
        ((MainActivity2) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity2) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        Button kor = view.findViewById(R.id.kor);
        Button eng = view.findViewById(R.id.eng);
        //현재 언어를 판별해줄 key를 저장함
        SharedPreferences cur_lan =getContext().getSharedPreferences("language", MODE_PRIVATE);
        SharedPreferences.Editor editor = cur_lan.edit();
        Load_Ini();
        kor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iniSection = iniFile.get("kor");
                //한국어로 설정을 했을때 현재 언어를 'kor' 로 저장함
                editor.putString("language","kor");
                editor.commit();
                Toast.makeText(getActivity(),cur_lan.getString("language",""),Toast.LENGTH_SHORT).show();
                Log.d("new_Tag", cur_lan.getString("language",""));
                Intent mIntent = new Intent(getActivity(), MainActivity2.class);
                startActivity(mIntent);
            }
        });
        eng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iniSection = iniFile.get("eng");
                //영어로 설정을 했을때 현재 언어를 'eng' 로 저장함
                editor.putString("language","eng");
                editor.commit();
                Intent mIntent = new Intent(getActivity(), MainActivity2.class);
                startActivity(mIntent);
            }
        });
        TextView language = (TextView)view.findViewById(R.id.secText);
        TextView info = (TextView)view.findViewById(R.id.info);
        language.setText(iniSection.get("lan"));
        info.setText(iniSection.get("apps_reenabled"));
        return view;

    }
    public Serializable Load_Ini() {
        AssetManager assetManager = getResources().getAssets();
        InputStream inputStream;
        try {
            inputStream = assetManager.open("ini/language.ini");
            Log.d("tag", "fileOpen");
            iniFile = new Ini(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return iniFile;
    }
}
