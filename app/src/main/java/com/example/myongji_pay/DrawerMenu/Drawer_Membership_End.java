package com.example.myongji_pay.DrawerMenu;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.example.myongji_pay.MainActivity2;
import com.example.myongji_pay.R;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;

import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;
public class Drawer_Membership_End extends Fragment {
    public static Drawer_Membership_End newInstance(){return new Drawer_Membership_End();}
    private View view;
    ImageView member_barCode;
    String membershipNum, membershipName;
    TextView barcode_num, membership_name;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.drawer_membership_barcode,container,false);
      Toolbar sub_toolbar = (Toolbar)view.findViewById(R.id.sub_toolbar);
        ((MainActivity2)getActivity()).setSupportActionBar(sub_toolbar);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        SharedPreferences sf = getActivity().getSharedPreferences("Membership", Context.MODE_PRIVATE);

        TextView title = (TextView)view.findViewById(R.id.secText);
        title.setText(iniSection.get("membership"));
        Bundle bundle = getArguments();

        //member_num = sf.getString("Member_num", "");
        membershipNum = bundle.getString("MembershipNum");
        membershipName = bundle.getString("Membership");

        membership_name = view.findViewById(R.id.membership);
        membership_name.setText(membershipName);

        member_barCode = view.findViewById(R.id.btnCreateBarcode);
        Bitmap barcode = createBarcode(membershipNum);
        member_barCode.setImageBitmap(barcode);
        barcode_num = view.findViewById(R.id.barcode_num);
        barcode_num.setText(membershipNum);
        return view;
    }
    public Bitmap createBarcode(String code){
        Bitmap bitmap = null;
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try{
            final int WIDTH = 600;
            final int HEIGHT = 200;
            BitMatrix bytemap = multiFormatWriter.encode(code, BarcodeFormat.CODE_93, WIDTH,HEIGHT);
            bitmap = Bitmap.createBitmap(WIDTH,HEIGHT,Bitmap.Config.ARGB_8888);
            for(int i = 0; i < WIDTH; ++i){
                for(int j = 0; j < HEIGHT; ++j){
                    bitmap.setPixel(i,j,bytemap.get(i,j)? Color.BLACK:Color.WHITE);
                }
            }
        }catch(Exception e){ }
        return bitmap;
    }
}
