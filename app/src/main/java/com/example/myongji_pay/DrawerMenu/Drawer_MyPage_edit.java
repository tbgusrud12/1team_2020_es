package com.example.myongji_pay.DrawerMenu;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.myongji_pay.MainActivity2;
import com.example.myongji_pay.R;
import com.example.myongji_pay.ResponseData.RegisterResponse;
import com.example.myongji_pay.User;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;
import static com.example.myongji_pay.RetrofitClient.getApiService;

public class Drawer_MyPage_edit extends Fragment {
    private View view;
    private boolean success = false;
    Drawer_MyPage drawer_myPage = new Drawer_MyPage();

    public static Drawer_MyPage_edit newInstance(){
        return new Drawer_MyPage_edit();
    }
    EditText phone_num, pwd ,pwd_check;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.drawer_mypage_edit,container,false);

        Toolbar sub_toolbar = (Toolbar)view.findViewById(R.id.sub_toolbar);
        ((MainActivity2)getActivity()).setSupportActionBar(sub_toolbar);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        //내 정보 수정 페이지 ini 파일 다국어 적용
        TextView my_info = (TextView)view.findViewById(R.id.secText);
        TextView username_info = (TextView)view.findViewById(R.id.username_info);
        EditText user_name = (EditText)view.findViewById(R.id.user_name);
        EditText id = (EditText)view.findViewById(R.id.id);
        TextView complete = (TextView)view.findViewById(R.id.complete_btn);
        TextView name = (TextView)view.findViewById(R.id.name);
        TextView ID = (TextView)view.findViewById(R.id.id_1);
        TextView phone_num_txt = (TextView)view.findViewById(R.id.phone_number);
        TextView pwd_txt = (TextView)view.findViewById(R.id.pwd_txt);
        TextView pwd_ck_txt = (TextView)view.findViewById(R.id.pwd_ck_txt);
        //EditText
        phone_num = view.findViewById(R.id.user_phone_number);
        pwd = view.findViewById(R.id.pwd);
        pwd_check = view.findViewById(R.id.pwd_check);

        my_info.setText(iniSection.get("info"));
        username_info.setText(User.userName+ iniSection.get("user_info"));
        complete.setText(iniSection.get("info"));
        name.setText(iniSection.get("name_hint"));
        ID.setText(iniSection.get("id_hint"));
        phone_num_txt.setText(iniSection.get("phone_number"));
        pwd_txt.setText(iniSection.get("pwd_hint"));
        pwd_ck_txt.setText(iniSection.get("pwd_conf"));

        user_name.setText(User.userName);
        id.setText(User.userId);
        //EditText
        pwd.setHint(iniSection.get("pwd_hint"));
        pwd_check.setHint(iniSection.get("pwd_hint"));



        /*수정완료 클릭 시 수정완료 토스트와 함께 내정보 화면으로 이동*/
        complete.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if (!pwd.getText().toString().equals(pwd_check.getText().toString()))
                    printToast(iniSection.get("toast_pwd_not_match"));
                else {
                    pwdRequest();

                }
            }
        });
        return view;
    }

    // 서버에 입력된 비밀번호 수정 request
    public void pwdRequest(){
        //params에 입력데이터 삽입
        HashMap<String, String> params = new HashMap<String, String>();
        Log.d("pwd", pwd.getText().toString());
        params.put("password", pwd.getText().toString());

        // PATCH 요청
        Call<RegisterResponse> call = getApiService(getActivity()).patch_pwd(params);
        call.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                if(response.isSuccessful()){
                    Log.d("patch_pwd", "success PATCH");
                    Bundle edit = new Bundle();
                    edit.putString("Phone_num", phone_num.getText().toString());
                    Log.d("Phome_num", String.valueOf(edit));
                    phone_num.setText(phone_num.getText().toString());
                    //drawer_myPage.setArguments(edit);
                    FragmentManager fm = getParentFragmentManager();
                    fm.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    ((MainActivity2)getActivity()).replaceFragment(Drawer_MyPage.newInstance(),edit);
                    printToast(iniSection.get("toast_user_info_changed"));
                }
                else{
                    Log.e("patch_pwd", "error code: " + response.code());
                    if(response.code() == 400)
                        printToast(iniSection.get("toast_not_entervalue"));
                    else if(response.code() == 500)
                        printToast(iniSection.get("toast_server_error"));
                    FragmentManager fm = getParentFragmentManager();
                    fm.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    ((MainActivity2)getActivity()).replaceFragment(Drawer_MyPage.newInstance(),null);
                    printToast(iniSection.get("toast_user_info_change_fail"));

                }
            }

            @Override
            public void onFailure(@NonNull Call<RegisterResponse> call, @NonNull Throwable t){
                Log.e("patch_pwd", "에러 : " + t.getMessage());
            }
        });
    }
    public void printToast(String data){
        Toast.makeText(this.getActivity(), data, Toast.LENGTH_SHORT).show();
    }
}
