package com.example.myongji_pay.DrawerMenu;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.myongji_pay.MainActivity2;
import com.example.myongji_pay.R;
import com.example.myongji_pay.ResponseData.MembershipResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;
import static com.example.myongji_pay.RetrofitClient.getApiService;

public class Drawer_Membership_Add extends Fragment {
    public static Drawer_Membership_Add newInstance(){return new Drawer_Membership_Add();}
    Button btnSave, btnCancle;
    RadioGroup radioGroup;
    EditText edit_memberName, edit_memberNum;
    String color;
    private View view;
    private Drawer_Membership membership = new Drawer_Membership();
    private Drawer_Membership_End membership_end = new Drawer_Membership_End();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.drawer_membership_add, container,false);
        //홈화면으로 넘어왔을 때 stack에 push를 해줌
        Toolbar sub_toolbar = (Toolbar)view.findViewById(R.id.sub_toolbar);
        ((MainActivity2)getActivity()).setSupportActionBar(sub_toolbar);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        //멤버쉽 추가 페이지 ini파일 다국어 설정
        TextView membership = (TextView)view.findViewById(R.id.secText);
        TextView member_name = (TextView)view.findViewById(R.id.membershipName);
        TextView member_num = (TextView)view.findViewById(R.id.membershipNum);
        TextView card_color = (TextView)view.findViewById(R.id.cardColor);
        EditText insert_name = (EditText)view.findViewById(R.id.insertName);
        EditText insert_num = (EditText)view.findViewById(R.id.insertNum);
        Button cancle = (Button)view.findViewById(R.id.btnCancle);
        Button save = (Button)view.findViewById(R.id.btnSave);

        membership.setText(iniSection.get("membership_add"));
        member_name.setText(iniSection.get("membership_name_mendatory"));
        member_num.setText(iniSection.get(""));
        card_color.setText(iniSection.get("card_color"));
        insert_name.setHint(iniSection.get("membership_name"));
        insert_num.setHint(iniSection.get("enter_card_without"));
        cancle.setText(iniSection.get("cancellation"));
        save.setText(iniSection.get("save"));


        btnSave = view.findViewById(R.id.btnSave);
        btnCancle = view.findViewById(R.id.btnCancle);

        radioGroup = view.findViewById(R.id.memberColorRadioGroup);

        edit_memberName = view.findViewById(R.id.insertName);
        edit_memberNum = view.findViewById(R.id.insertNum);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.btn_black:
                        color = "black";
                        break;
                    case R.id.btn_red:
                        color = "red";
                        break;
                    case R.id.btn_orange:
                        color = "orange";
                        break;
                    case R.id.btn_yellow:
                        color = "yellow";
                        break;
                    case R.id.btn_green:
                        color = "green";
                        break;
                    case R.id.btn_navy:
                        color = "navy";
                        break;
                    case R.id.btn_purple:
                        color = "purple";
                        break;
                    default:;
                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edit_memberName.getText().toString().replace(" ", "").equals("")){
                    printToast(iniSection.get("toast_enter_membership"));
                    edit_memberName.requestFocus();
                    showKeypad();
                }
                else if(edit_memberNum.getText().toString().replace(" ", "").equals("")){
                    printToast(iniSection.get("toast_enter_memcard"));
                    edit_memberNum.requestFocus();
                    showKeypad();
                }
                //색상 선택 안하면
                else if(radioGroup.getCheckedRadioButtonId() == -1){
                    printToast(iniSection.get("toast_selet_cardcolor"));
                }
                else{
                    sendRequest();

                }
            }
        });
        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getParentFragmentManager();
                fm.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
                ((MainActivity2)getActivity()).replaceFragment(Drawer_Membership.newInstance(),null);
            }
        });
        return view;
    }
    public void sendRequest(){
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("membershipName", edit_memberName.getText().toString());
        params.put("membershipNumber", edit_memberNum.getText().toString());
        params.put("membershipColor", color);

        //POST 요청
        Call<MembershipResponse> call = getApiService(getActivity()).postMembership(params);
        call.enqueue(new Callback<MembershipResponse>(){
            @Override
            public void onResponse(Call<MembershipResponse> call, Response<MembershipResponse> response) {
                if(response.isSuccessful()){
                    Log.d("postMembership", "success POST");
                    printToast("성공");
                    FragmentManager fm = getParentFragmentManager();
                    fm.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    ((MainActivity2)getActivity()).replaceFragment(Drawer_Membership.newInstance(), null);
                }
                else{
                    Log.e("postMembership", "error code: " + response.code());
                    if(response.code() == 400)
                        printToast(iniSection.get("toast_mem_cardnumber"));
                }

            }
            @Override
            public void onFailure(@NonNull Call<MembershipResponse> call, @NonNull Throwable t){
                Log.e("postMembership", "에러 : " + t.getMessage());
            }
        });
    }
    public void printToast(String data){
        Toast.makeText(getActivity(), data, Toast.LENGTH_SHORT).show();
    }
    public void showKeypad(){
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }
}
