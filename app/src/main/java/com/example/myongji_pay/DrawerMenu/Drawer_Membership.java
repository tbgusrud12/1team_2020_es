package com.example.myongji_pay.DrawerMenu;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.example.myongji_pay.MainActivity2;
import com.example.myongji_pay.MyMembershipAdapter;
import com.example.myongji_pay.R;
import com.example.myongji_pay.ResponseData.MembershipResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;
import static com.example.myongji_pay.RetrofitClient.getApiService;

public class Drawer_Membership extends Fragment {
    public static Drawer_Membership newInstance(){return new Drawer_Membership();}
    private View view;

    private ArrayList<String> array_cardCompany;
    private ArrayList<String> array_cardNumber;
    private ArrayList<String> array_cardColor;
    private ArrayList<String> array_cardId;


    private ListView listView;
    private MyMembershipAdapter membershipAdapter;
    Context context;
    Button addMembership,add;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.drawer_membership,container,false);

        Toolbar sub_toolbar = (Toolbar)view.findViewById(R.id.sub_toolbar);
        ((MainActivity2)getActivity()).setSupportActionBar(sub_toolbar);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        //멤버쉽 페이지 ini파일 다국어 설정
        TextView membership = (TextView)view.findViewById(R.id.secText);
        addMembership = view.findViewById(R.id.add_membership);
        membership.setText(iniSection.get("membership"));
        addMembership.setText(iniSection.get("membership_add"));
        //context = getActivity().getApplicationContext();
        context = getContext();

        listView = (ListView) view.findViewById(R.id.list_card);

        array_cardCompany = new ArrayList<>();
        array_cardNumber = new ArrayList<>();
        array_cardColor = new ArrayList<>();
        array_cardId = new ArrayList<>();

        //GET 요청
        Call<ArrayList<MembershipResponse>> call = getApiService(getActivity()).getMemberships();
        call.enqueue(new Callback<ArrayList<MembershipResponse>>(){
            @Override
            public void onResponse(Call<ArrayList<MembershipResponse>> call, Response<ArrayList<MembershipResponse>> response) {
                if(response.isSuccessful()){
                    List<MembershipResponse> responseList = response.body();
                    for(MembershipResponse item : responseList){
                        Log.d("getMembership","Membership name : " + item.getMembershipName() + ", number : " + item.getMembershipNumber() + ", nick : " + item.getMembershipNumber() + ", color : " + item.getMembershipColor());
                        putMembershipToList(item.getMembershipName(), item.getMembershipNumber(), item.getMembershipColor(), item.getId());
                    }
                    membershipAdapter = new MyMembershipAdapter(context, array_cardCompany, array_cardNumber, array_cardColor, array_cardId, listView);
                    listView.setAdapter(membershipAdapter);
                    Log.d("getMembership", "success GET");
                }
                else{
                    Log.e("getMembership", "error code: " + response.code());
                    if(response.code() == 500)
                        printToast(iniSection.get("toast_server_error"));
                }
            }
            @Override
            public void onFailure(@NonNull Call<ArrayList<MembershipResponse>> call, @NonNull Throwable t){
                Log.e("getMembership", "에러 : " + t.getMessage());
            }
        });

        add = view.findViewById(R.id.add);

        addMembership.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //리스트 추가 해보기
                ((MainActivity2)getActivity()).replaceFragment(Drawer_Membership_Add.newInstance(),null);
            }
        });
        return view;
    }


    //멤버십 정보를 읽어 ArrayList에 추가
    void putMembershipToList(String mName, String mNumber, String mColor, String mId){
        array_cardCompany.add(mName);
        array_cardNumber.add(mNumber);
        array_cardColor.add(mColor);
        array_cardId.add(mId);
    }

    public void printToast(String data){
        Toast.makeText(getActivity(), data, Toast.LENGTH_SHORT).show();
    }
}
