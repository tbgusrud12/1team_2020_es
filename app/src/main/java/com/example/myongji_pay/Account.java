package com.example.myongji_pay;

public class Account {
   String bank;
   String account_num;
   String fintechUseNum;
   String account_balance;

    public Account(String bank, String account_num, String fintechUseNum, String account_balance) {
        this.bank = bank;
        this.account_num = account_num;
        this.fintechUseNum = fintechUseNum;
        this.account_balance = account_balance;
    }

    public Account(String bank, String account_num, String fintechUseNum) {
        this.bank = bank;
        this.account_num = account_num;
        this.fintechUseNum = fintechUseNum;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getAccount_num() {
        return account_num;
    }

    public void setAccount_num(String account_num) {
        this.account_num = account_num;
    }

    public String getFintechUseNum() {
        return fintechUseNum;
    }

    public void setFintechUseNum(String fintechUseNum) {
        this.fintechUseNum = fintechUseNum;
    }

    public String getAccount_balance() {
        return account_balance;
    }

    public void setAccount_balance(String account_balance) {
        this.account_balance = account_balance;
    }
}
