package com.example.myongji_pay.BottomMenu;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.myongji_pay.DrawerMenu.Drawer_Details_check;
import com.example.myongji_pay.DrawerMenu.Drawer_Membership;
import com.example.myongji_pay.MainActivity2;
import com.example.myongji_pay.R;
import com.example.myongji_pay.ResponseData.CountResponse;
import com.example.myongji_pay.ResponseData.TransactionAllResponse;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;
import static com.example.myongji_pay.RetrofitClient.getApiService;

public class _Home extends Fragment {
    private View view;
    public static _Home newInstance(){return new _Home();}
    Button btnCard, btnAccount, btnMember, btnMore;
    TextView card, account, membership, month_expense, my_wallet, usage, usage1, usage2, usage1_amt, usage2_amt;
    ArrayList<String> labels;
    ArrayList<Entry> entries;
    LineDataSet lineDataSet;
    private LineChart chart;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.mainhome,container,false);

        entries = new ArrayList<>();

        chart = (LineChart)view.findViewById(R.id.chart);
        chart.setNoDataText("loading...");

        XAxis xAxis = chart.getXAxis(); //X축 설정
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);  //X축 위치 설정 : bottom
        xAxis.setTextSize(10f); //X축 텍스트 크기 설정 : 10f
        xAxis.setDrawGridLines(false);  //X축 그리드 라인 설정 : false

        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setEnabled(false);

        YAxis rightAxis = chart.getAxisRight();
        leftAxis.setEnabled(false);


        btnCard = view.findViewById(R.id.cardBtn);
        btnAccount = view.findViewById(R.id.accountBtn);
        btnMember = view.findViewById(R.id.membership);
        btnMore = view.findViewById(R.id.btnMore);

        card = view.findViewById(R.id.cardText);
        account = view.findViewById(R.id.accountText);
        membership = view.findViewById(R.id.membershipText);
        my_wallet = view.findViewById(R.id.title);
        month_expense = view.findViewById(R.id.second_title);
        usage = view.findViewById(R.id.third_title);

        usage1 = view.findViewById(R.id.usage);
        usage2 = view.findViewById(R.id.usage2);
        usage1_amt = view.findViewById(R.id.usage_amt);
        usage2_amt = view.findViewById(R.id.usage2_amt);

        btnMore.setText(iniSection.get("see_more"));
        card.setText(iniSection.get("title_MyCard"));
        account.setText(iniSection.get("title_MyAccount"));
        membership.setText(iniSection.get("member"));
        usage.setText(iniSection.get("usage"));
        my_wallet.setText(iniSection.get("my_wallet"));
        month_expense.setText(iniSection.get("month_expenditure"));

        getCountRequest();
        getTranGraphRequest();
        getTranRecentRequest();

        btnCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity2)getActivity()).replaceFragment(_MyCard.newInstance(),null);
            }
        });
        btnAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity2)getActivity()).replaceFragment(_Send.newInstance(),null);
            }
        });
        btnMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity2)getActivity()).replaceFragment(Drawer_Membership.newInstance(),null);
            }
        });
        btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity2)getActivity()).replaceFragment(Drawer_Details_check.newInstance(),null);
            }
        });

        return view;
    }

    String getMonth(){
        long now = System.currentTimeMillis();
        Date mDate = new Date(now);
        SimpleDateFormat simple = new SimpleDateFormat("MM");
        String month = simple.format(mDate);

        return month;
    }
    String getDay(){
        long now = System.currentTimeMillis();
        Date mDate = new Date(now);
        SimpleDateFormat simple = new SimpleDateFormat("dd");
        String day = simple.format(mDate);

        return day;
    }


    private void getCountRequest(){
        Call<CountResponse> call = getApiService(getActivity()).getCount();
        call.enqueue(new Callback<CountResponse>(){
            @Override
            public void onResponse(Call<CountResponse> call, Response<CountResponse> response) {
                if(response.isSuccessful()){
                    btnCard.setText(response.body().getCard());
                    btnAccount.setText(response.body().getAccount());
                    btnMember.setText(response.body().getMembership());
                    Log.d("getCount", "success GET");

                }
                else{
                    Log.e("getCount", "error code: " + response.code());
                    if(response.code() == 500)
                        printToast(iniSection.get("toast_server_error"));
                }
            }
            @Override
            public void onFailure(@NonNull Call<CountResponse> call, @NonNull Throwable t){
                Log.e("getCount", "에러 : " + t.getMessage());
            }
        });
    }

    private void getTranGraphRequest() {
        Call<ArrayList<TransactionAllResponse>> call = getApiService(getActivity()).getTransactionGraph("2020"+getMonth()+"01", "2020"+getMonth()+getDay());

        call.enqueue(new Callback<ArrayList<TransactionAllResponse>>(){
            @Override
            public void onResponse(Call<ArrayList<TransactionAllResponse>> call, Response<ArrayList<TransactionAllResponse>> response) {
                if(response.isSuccessful()){
                    List<TransactionAllResponse> responseList = response.body();
                    for(TransactionAllResponse item : responseList){
                        entries.add(new Entry(Integer.parseInt(item.getTranDate().substring(6,8)), Float.parseFloat(item.getAmount())));
                    }
                    //Collections.reverse(entries);
                    Log.d("##^#$$#", entries.toString());
                    Log.d("getTransactionAll", "success GET");
                    //refreshFrag();\
                    lineDataSet = new LineDataSet(entries, "지출");
                    //lineDataSet.setColor(Color.argb(0x88,0xFF,0xC9, 0x0E));
                    lineDataSet.setDrawFilled(true);
                    lineDataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                    lineDataSet.setCubicIntensity(0.2f);
                    LineData lineData = new LineData(lineDataSet);
                    chart.setData(lineData);
                    chart.invalidate();
                }
                else{
                    Log.e("getTransactionAll", "error code: " + response.code());
                    if(response.code() == 500)
                        printToast(iniSection.get("toast_server_error"));
                }
            }
            @Override
            public void onFailure(@NonNull Call<ArrayList<TransactionAllResponse>> call, @NonNull Throwable t){
                Log.e("getTransactionAll", "에러 : " + t.getMessage());
            }
        });
    }

    private void getTranRecentRequest(){
        Call<ArrayList<TransactionAllResponse>> call = getApiService(getActivity()).getTransactionAll("2020" + getMonth()+"01", "2020" + getMonth()+getDay());
        call.enqueue(new Callback<ArrayList<TransactionAllResponse>>(){
            @Override
            public void onResponse(Call<ArrayList<TransactionAllResponse>> call, Response<ArrayList<TransactionAllResponse>> response) {
                if(response.isSuccessful()) {
                    if (response.body().toString() != "[]") {
                        usage1.setText(response.body().get(0).getContent() + " (" + response.body().get(0).getInoutType() + ")");
                        usage1_amt.setText(response.body().get(0).getAmount() + " "+iniSection.get("won"));
                        usage2.setText(response.body().get(1).getContent() + " (" + response.body().get(1).getInoutType() + ")");
                        usage2_amt.setText(response.body().get(1).getAmount() + " "+iniSection.get("won"));
                    }
                    else{
                        usage1.setText(iniSection.get("toast_errors_to"));
                        usage1_amt.setText("-");
                        usage2.setText(iniSection.get("toast_errors_to"));
                        usage2_amt.setText("-");
                    }

                    Log.d("getTranRecent", "success GET");

                }
                else{
                    Log.e("getTranRecent", "error code: " + response.code());
                    if(response.code() == 500)
                        printToast(iniSection.get("toast_server_error"));
                }
            }
            @Override
            public void onFailure(@NonNull Call<ArrayList<TransactionAllResponse>> call, @NonNull Throwable t){
                Log.e("getTranRecent", "에러 : " + t.getMessage());
            }
        });
    }


    public void printToast(String data){
        Toast.makeText(getContext(), data, Toast.LENGTH_SHORT).show();
    }
}
