package com.example.myongji_pay.BottomMenu;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.example.myongji_pay.FingerPrinter;
import com.example.myongji_pay.MainActivity2;
import com.example.myongji_pay.R;
import com.example.myongji_pay.ResponseData.CardResponse;
import com.google.zxing.ResultPoint;
import com.google.zxing.integration.android.IntentIntegrator;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;
import static com.example.myongji_pay.RetrofitClient.getApiService;

public class _Payment extends Fragment {
    private View view;
    private String strs;
    private IntentIntegrator qrScan;
    private CaptureManager capture;
    private ArrayList<String> array_bank;
    private ArrayList<String> array_cardNumber;
    private DecoratedBarcodeView barcodeScannerView;
    public static _Payment newInstance(){
        return new _Payment();
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.bottom_payment, container, false);

        //상단 툴바 설정
        Toolbar sub_toolbar = (Toolbar) view.findViewById(R.id.sub_toolbar);
        ((MainActivity2) getActivity()).setSupportActionBar(sub_toolbar);
        ((MainActivity2) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity2) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        //결제 페이지 ini파일 다국어 설정
        TextView payment = (TextView) view.findViewById(R.id.secText);
        payment.setText(iniSection.get("title_Payment"));
        List<String> spinnerArray = new ArrayList<String>();
        array_bank = new ArrayList<>();
        array_cardNumber = new ArrayList<>();

        //GET 요청
        Call<ArrayList<CardResponse>> call = getApiService(getActivity()).getCards();
        call.enqueue(new Callback<ArrayList<CardResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<CardResponse>> call, Response<ArrayList<CardResponse>> response) {
                if (response.isSuccessful()) {
                    List<CardResponse> responseList = response.body();
                    for (CardResponse item : responseList) {
                        Log.d("getCard", "card name : " + item.getBank() + ", number : " + item.getCardNumber_d() + ", color : " + item.getCardColor());
                        spinnerArray.add(item.getBank() + " " + item.getCardNumber_d() + "- ****");
                        ArrayAdapter <String >adapter = new ArrayAdapter<>(getContext(), R.layout.spinner_item, spinnerArray);
                        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                        Spinner spinner = view.findViewById(R.id.account_spinner);
                        spinner.setAdapter(adapter);

                        /* 아이템이 선택되었을 때의 이벤트 처리 리스너 설정 */
                        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                strs= (String)spinner.getSelectedItem();

                                Log.i("i", "Spinner selected item = "+strs);


                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });


                        Log.d("getCard", "success GET");
                        Log.d("getCard", spinnerArray + "d");
                    }
                } else {
                    Log.e("getCard", "error code: " + response.code());
                    if (response.code() == 500)
                        printToast(".");
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<CardResponse>> call, @NonNull Throwable t) {
                Log.e("getCard", "에러 : " + t.getMessage());
            }
        });

        Button next = view.findViewById(R.id.btnNext);
        next.setText(iniSection.get("next"));
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), FingerPrinter.class);
                intent.putExtra("option", "payment");
                startActivity(intent);
            }
        });
        //바코드 스캐너 등록
        barcodeScannerView = (DecoratedBarcodeView)view.findViewById(R.id.BarcodeScanner);
        capture = new CaptureManager(getActivity(),barcodeScannerView);
        capture.initializeFromIntent(getActivity().getIntent(),savedInstanceState);
        capture.decode();
        barcodeScannerView.decodeContinuous((new BarcodeCallback() {
            @Override
            public void barcodeResult(BarcodeResult result) {
                readBarcode(result.toString());
            }

            @Override
            public void possibleResultPoints(List<ResultPoint> resultPoints) {

            }
        }));

        return view;
    }


    //바코드 카메라 스캔
    public void onResume(){
        super.onResume();
        capture.onResume();
    }
    public void onPause(){
        super.onPause();
        capture.onPause();

    }
    public void onDestroy(){
        super.onDestroy();
        capture.onDestroy();
    }
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        capture.onSaveInstanceState(outState);
    }
    public void readBarcode(String barcode){
        Toast.makeText(getActivity(),barcode,Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getActivity(), FingerPrinter.class);
        intent.putExtra("option", "payment");
        startActivity(intent);
    }
    //카드정보 입력하면 출력
    void printCard(String bank, String cardNumber){
        array_bank.add(bank);
        array_cardNumber.add(cardNumber+ "-****");

    }
    public void printToast(String data){
        Toast.makeText(getActivity(), data, Toast.LENGTH_SHORT).show();
    }
}

