package com.example.myongji_pay.BottomMenu;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.example.myongji_pay.AddCard;
import com.example.myongji_pay.MainActivity2;
import com.example.myongji_pay.MyCardAdapter2;
import com.example.myongji_pay.R;
import com.example.myongji_pay.ResponseData.CardResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.myongji_pay.RetrofitClient.getApiService;
import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;
public class _MyCard extends Fragment {
    private View view;

    private ArrayList<String> array_bank;
    private ArrayList<String> array_cardNumber;
    private ArrayList<String> array_cardColor;
    private ArrayList<String> array_cardId;

    private ListView listView;
    private MyCardAdapter2 cardAdapter;
    Context context;

    public static _MyCard newInstance(){return new _MyCard();}
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.bottom_mycard, container, false);

        //상단 툴바 설정
        Toolbar sub_toolbar = (Toolbar)view.findViewById(R.id.sub_toolbar);
        ((MainActivity2)getActivity()).setSupportActionBar(sub_toolbar);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        //내 카드 페이지 ini파일 다국어 설정
        Button addbtn = (Button)view.findViewById(R.id.add_card);
        TextView myCard = (TextView)view.findViewById(R.id.secText);
        myCard.setText(iniSection.get("title_MyCard"));
        addbtn.setText(iniSection.get("add_card"));

        context = getActivity().getApplicationContext();
        //카드의 배열을 확일할 리스트뷰
        listView = (ListView) view.findViewById(R.id.list_card);

        array_bank = new ArrayList<>();
        array_cardNumber = new ArrayList<>();
        array_cardColor = new ArrayList<>();
        array_cardId = new ArrayList<>();

        //GET 요청
        Call<ArrayList<CardResponse>> call = getApiService(getActivity()).getCards();
        call.enqueue(new Callback<ArrayList<CardResponse>>(){
            @Override
            public void onResponse(Call<ArrayList<CardResponse>> call, Response<ArrayList<CardResponse>> response) {
                if(response.isSuccessful()){
                    List<CardResponse> responseList = response.body();
                    for(CardResponse item : responseList){
                        Log.d("getCard","card name : " + item.getBank() + ", number : " + item.getCardNumber_d() +  ", color : " + item.getCardColor());
                        printCard(item.getBank(), item.getCardNumber_d(), item.getCardColor(), item.getId());
                    }
                    cardAdapter = new MyCardAdapter2(context, array_bank, array_cardNumber, array_cardColor, array_cardId, listView);
                    listView.setAdapter(cardAdapter);
                    Log.d("getCard", "success GET");
                }
                else{
                    Log.e("getCard", "error code: " + response.code());
                    if(response.code() == 500)
                        printToast(".");
                }
            }
            @Override
            public void onFailure(@NonNull Call<ArrayList<CardResponse>> call, @NonNull Throwable t){
                Log.e("getCard", "에러 : " + t.getMessage());
            }
        });


//        cardAdapter = new MyCardAdapter2(context, array_bank, array_cardNumber, array_cardColor, array_cardId);
//        listView.setAdapter(cardAdapter);


        addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity2)getActivity()).replaceFragment(AddCard.newInstance(),null);
            }
        });
        return view;
    }

    //카드정보 입력하면 출력
    void printCard(String bank, String cardNumber, String cardColor, String cardId){
        array_bank.add(bank);
        array_cardNumber.add(cardNumber+ "-****");
        array_cardColor.add(cardColor);
        array_cardId.add(cardId);
    }
    public void printToast(String data){
        Toast.makeText(getActivity(), data, Toast.LENGTH_SHORT).show();
    }
}