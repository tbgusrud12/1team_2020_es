package com.example.myongji_pay.BottomMenu;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myongji_pay.Account;
import com.example.myongji_pay.MainActivity2;
import com.example.myongji_pay.MyAccountAdapter;
import com.example.myongji_pay.NewAccountIdentifyWebview;
import com.example.myongji_pay.R;
import com.example.myongji_pay.ResponseData.AccountResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;
import static com.example.myongji_pay.RetrofitClient.getApiService;

public class _Send extends Fragment {
    private View view;
    Button next, add_account;
    private ArrayList<Account> dataList;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    TextView send;


    public static _Send newInstance(){
        return new _Send();
    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.bottom_send, container, false);
        //화면 띄울시 stack에 push
//        Fragment currentFragment = MainActivity2.fm.findFragmentById(R.id.main_frame);
//        MainActivity2.fragmentStack.push(currentFragment);
        //상단 툴바 설정
        Toolbar sub_toolbar = (Toolbar)view.findViewById(R.id.sub_toolbar);
        ((MainActivity2)getActivity()).setSupportActionBar(sub_toolbar);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        dataList = new ArrayList<>();

        //계좌목록 조회 후 잔액조회
        accountListRequest();

        //송금 페이지 ini파일 다국어 설정
        send = (TextView)view.findViewById(R.id.secText) ;
        send.setText(iniSection.get("title_Send"));
        //wire.setText(iniSection.get("title_Send"));

        add_account = view.findViewById(R.id.add_account);
        add_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), NewAccountIdentifyWebview.class);
                startActivity(intent);
                ((MainActivity2)getActivity()).replaceFragment(_Send.newInstance(),null);
            }
        });





        return view;
    }


    public void accountListRequest(){
        //GET 요청
        Call<ArrayList<AccountResponse>> call = getApiService(getActivity()).getAccountList();
        call.enqueue(new Callback<ArrayList<AccountResponse>>(){
            @Override
            public void onResponse(Call<ArrayList<AccountResponse>> call, Response<ArrayList<AccountResponse>> response) {
                if(response.isSuccessful()){
                    List<AccountResponse> responseList = response.body();
                    for(AccountResponse item : responseList){
                        Log.d("getAccountList","bank : " + item.getBankName() + ", account_number : " + item.getAccountNumMasked() +  ", fintechNum : " + item.getFintechUseNum());
                        //은행, 계좌번호, fintechUserNumber만 먼저 넣고 잔액 조회 후 잔액 삽입
                        dataList.add(new Account(item.getBankName(), item.getAccountNumMasked(), item.getFintechUseNum()));
                    }
                    Log.d("getAccountList", "success GET");
                    accountBalanceRequest();
                }
                else{
                    Log.e("getAccountList", "error code: " + response.code());
                    if(response.code() == 500)
                        printToast(iniSection.get("toast_server_error"));
                }
            }
            @Override
            public void onFailure(@NonNull Call<ArrayList<AccountResponse>> call, @NonNull Throwable t){
                Log.e("getAccountList", "에러 : " + t.getMessage());
            }
        });
        //

    }

    public void accountBalanceRequest(){
        for(Account account: dataList){
            //GET 요청
            Call<String> call = getApiService(getActivity()).getAccountBalance(account.getFintechUseNum());
            call.enqueue(new Callback<String>(){
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if(response.isSuccessful()){
                        //Log.d("getAccountBalance", response.body());
                        account.setAccount_balance(response.body());

                        Log.d("getAccountBalance", "success GET");
                        showFrag();
                    }
                    else{
                        Log.e("getAccountBalance", "error code: " + response.code());
                        if(response.code() == 500)
                            printToast(iniSection.get("toast_server_error"));
                    }
                }
                @Override
                public void onFailure(@NonNull Call<String> call, @NonNull Throwable t){
                    Log.e("getAccountBalance", "에러 : " + t.getMessage());
                }
            });
        }
    }
    public void showFrag(){
        recyclerView  = (RecyclerView)view.findViewById(R.id.recyclerview_account_list);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        ((LinearLayoutManager) layoutManager).setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager); // LayoutManager 등록
        adapter = new MyAccountAdapter(getContext(), dataList);
        recyclerView.setAdapter(adapter);


    }
    public void printToast(String data){
        Toast.makeText(getActivity(), data, Toast.LENGTH_SHORT).show();
    }
}
