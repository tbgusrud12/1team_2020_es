package com.example.myongji_pay;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myongji_pay.ResponseData.TransactionAllResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;
import static com.example.myongji_pay.RetrofitClient.getApiService;
public class TransactionDetail extends AppCompatActivity implements TransactionDetailAdpater.OnItemClickListener {

    private TransactionDetailAdpater adapter;


    private String fromDate, toDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transaction_detail);

        fromDate = "20201101";
        toDate = "20201124";

        RecyclerView mTimeRecyclerView = (RecyclerView) findViewById(R.id.transactions_list);
        mTimeRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mTimeRecyclerView.setLayoutManager(layoutManager);

        adapter = new TransactionDetailAdpater(getDataset());
        adapter.setOnItemClickListener(this);
        mTimeRecyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(int position) {
        Toast.makeText(this, adapter.getItem(position).getTitle(), Toast.LENGTH_SHORT).show();
    }

    private ArrayList<TransactionData> getDataset() {
        ArrayList<TransactionData> dataset = new ArrayList<>();

        //GET 요청
        Call<ArrayList<TransactionAllResponse>> call = getApiService(this).getTransactionAll(fromDate, toDate);
        call.enqueue(new Callback<ArrayList<TransactionAllResponse>>(){
            @Override
            public void onResponse(Call<ArrayList<TransactionAllResponse>> call, Response<ArrayList<TransactionAllResponse>> response) {
                if(response.isSuccessful()){
                    List<TransactionAllResponse> responseList = response.body();
                    for(TransactionAllResponse item : responseList){
                        Log.d("getTransactionAll","bank name : " + item.getBankName() + ", tran date : " + item.getTranDate() + ", tran time : " + item.getTranTime() + ", inout type : " + item.getInoutType() + ", content : " + item.getContent() + ", amount : " + item.getAmount());
                        dataset.add(new TransactionData(item.getContent(), item.getAmount() + iniSection.get("won"), getTime(item.getTranTime()), item.getBankName(), item.getInoutType(), Integer.parseInt(item.getTranDate().substring(0,4)), Integer.parseInt(item.getTranDate().substring(4,6)), Integer.parseInt(item.getTranDate().substring(6,8))));
                    }
                    //membershipAdapter = new MyMembershipAdapter(context, array_cardCompany, array_cardNumber, array_cardColor, array_cardId);
                    //listView.setAdapter(membershipAdapter);
                    Log.d("getTransactionAll", "success GET");
                }
                else{
                    Log.e("getTransactionAll", "error code: " + response.code());
                    if(response.code() == 500)
                        printToast(iniSection.get("toast_server_error"));
                }
            }
            @Override
            public void onFailure(@NonNull Call<ArrayList<TransactionAllResponse>> call, @NonNull Throwable t){
                Log.e("getTransactionAll", "에러 : " + t.getMessage());
            }
        });

        return dataset;
    }

    String getTime(String t){
        return t.substring(0,2) + ":" + t.substring(2,4);
    }

    public void printToast(String data){
        Toast myToast = Toast.makeText(this.getApplicationContext(), data, Toast.LENGTH_SHORT);
        myToast.show();
    }
}
