package com.example.myongji_pay;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import com.example.myongji_pay.BottomMenu._MyCard;
import com.example.myongji_pay.ResponseData.CardResponse;
import com.example.myongji_pay.ResponseData.LoginResponse;

import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.Executor;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.myongji_pay.RetrofitClient.getApiService;
import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;
public class FingerPrinter extends AppCompatActivity {
    private TextView tv_message;
    private Button btnCancle;

    private Executor executor;
    private BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;
    private Cipher cipher;
    private SecretKey secretKey;
    private static final String KEY_NAME = "example_key";
    private static final String plain_text ="encrypto";
    EditText pw[] = new EditText[6];
    Integer[] edit_id = {R.id.pw1,R.id.pw2,R.id.pw3,R.id.pw4,R.id.pw5,R.id.pw6};
    String type, pwd;
    private boolean success = false;
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.fingerprint);
        TextView password = (TextView)findViewById(R.id.password);
        btnCancle = (Button)findViewById(R.id.btnCancle);
        //지문인식 다국어 적용
        password.setText(iniSection.get("pwd_hint"));
        btnCancle.setText(iniSection.get("cancellation"));
        //지문 클릭
        Intent intent = getIntent();
        final String option = intent.getExtras().getString("option");
        type = option;
        //잠금 화면 구현
        setShowWhenLocked(true);
        setTurnScreenOn(true);
        //지문인식 프롬트 메시지 띄워줌
        SharedPreferences sf = getSharedPreferences("permission", MODE_PRIVATE);
        String permission = sf.getString("permission","");
        if(permission.equals("allowed")){
            generateKey();
        }
        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        for(int i = 0; i < 6; i++){
            pw[i] = (EditText)findViewById(edit_id[i]);
            pw[0].requestFocus();
            final int finalI = i;
            pw[i].addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
                @Override
                public void afterTextChanged(Editable s) {
                    if(finalI < 5) {
                        pw[finalI + 1].requestFocus();
                    }else {
                        pwd = pw[0].getText().toString()+pw[1].getText().toString()+pw[2].getText().toString()+
                                pw[3].getText().toString()+pw[4].getText().toString()+pw[5].getText().toString();
                        pwdCheckRequest();
                        for(int i = 0; i < 6; i++){
                            pw[i].getText().clear();
                        }
                        pw[0].requestFocus();
                    }
                }
            });
        }
    }
    public void pwdCheckRequest(){
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("AppPwd", pwd);
        Call<String> call = getApiService(getApplicationContext()).postPwdCheck(params);
        call.enqueue(new Callback<String>(){
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    success = true;
                    auth_success();
                }
                else{
                    if(response.code()==409)
                        printToast(iniSection.get("toast_pwd_not_match"));
                    success = false;
                }

            }
            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t){
                success = false;
            }
        });
    }
    public void printToast(String data){
        Toast.makeText(this.getApplicationContext(), data, Toast.LENGTH_SHORT).show();
    }
    public void generateKey(){
        biometricPrompt = createBiometricPrompt();
        try {
            generateSecretKey(new KeyGenParameterSpec.Builder(
                    KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .setUserAuthenticationRequired(true)
                    // Invalidate the keys if the user has registered a new biometric
                    // credential, such as a new fingerprint. Can call this method only
                    // on Android 7.0 (API level 24) or higher. The variable
                    // "invalidatedByBiometricEnrollment" is true by default.
                    .setInvalidatedByBiometricEnrollment(true)
                    .build());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        //암호화를 통합하는 생체 인식 인증 워크플로를 시작
        try { cipher = getCipher(); }
        catch (Exception e) { e.printStackTrace(); }
        try { secretKey = getSecretKey(); }
        catch (Exception e) { e.printStackTrace(); }
        try { cipher.init(Cipher.ENCRYPT_MODE, secretKey); }
        catch (InvalidKeyException e) { e.printStackTrace(); }

        promptInfo = createPromptInfo();
        biometricPrompt.authenticate(promptInfo, new BiometricPrompt.CryptoObject(cipher));
    }
    private BiometricPrompt createBiometricPrompt(){
        executor = ContextCompat.getMainExecutor(this);
        BiometricPrompt callback = new BiometricPrompt(FingerPrinter.this,
                executor, new BiometricPrompt.AuthenticationCallback(){
            @Override
            public void onAuthenticationError(int errorCode,
                                              @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                Toast.makeText(getApplicationContext(),
                        iniSection.get("toast_auth_err")+": " + errString, Toast.LENGTH_SHORT)
                        .show();
            }
            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                Objects.requireNonNull(result);
                byte[] encryptedInfo = {};
               //사용자의 지문을 암호화
                try {
                    encryptedInfo = result.getCryptoObject().getCipher().doFinal(plain_text.getBytes(Charset.defaultCharset()));
                } catch (BadPaddingException e) {
                    e.printStackTrace();
                } catch (IllegalBlockSizeException e) {
                    e.printStackTrace();
                }
                Log.d("MY_APP_TAG", "Encrypted information: " +
                        Arrays.toString(encryptedInfo));
                Toast.makeText(getApplicationContext(), iniSection.get("toast_auth_success"),
                        Toast.LENGTH_SHORT)
                        .show();
                auth_success();
            }
            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                Toast.makeText(getApplicationContext(), iniSection.get("toast_auth_failed"),
                        Toast.LENGTH_SHORT)
                        .show();
            }
        });
       return callback;
    }
    private BiometricPrompt.PromptInfo createPromptInfo(){
        promptInfo = new BiometricPrompt.PromptInfo.Builder()
                .setTitle(iniSection.get("dialog_biometric"))
                .setSubtitle(iniSection.get("dialog_use_biometric"))
                .setNegativeButtonText(iniSection.get("dialog_use_pwd"))
                .build();

        return promptInfo;
    }
    private void generateSecretKey(KeyGenParameterSpec keyGenParameterSpec) throws Exception {
        KeyGenerator keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        keyGenerator.init(keyGenParameterSpec);
        keyGenerator.generateKey();
    }
    private SecretKey getSecretKey()  throws Exception{
        KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
        // Before the keystore can be accessed, it must be loaded.
        keyStore.load(null);
        return ((SecretKey)keyStore.getKey(KEY_NAME, null));
    }
    private Cipher getCipher()  throws Exception{
        return Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/"
                + KeyProperties.BLOCK_MODE_CBC + "/"
                + KeyProperties.ENCRYPTION_PADDING_PKCS7);
    }
    public void auth_success(){
        if(type.equals("payment")) {
            Intent intent = new Intent(getApplicationContext(), Payment_End.class);
            startActivity(intent);
        }else if(type.equals("wire")){
            Intent intent = new Intent(getApplicationContext(), WireEnd.class);
            startActivity(intent);
        }else if(type.equals("login")){
            Call<LoginResponse> call = getApiService(this).postAutoLogIn();
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    if(response.isSuccessful()){
                        Log.d("postAutoLogin", "success Post");
                        User.id = response.body().getId();
                        User.userId = response.body().getUserId();
                        User.userName = response.body().getUsername();
                        User.access_token = response.body().getAccess_token();
                        Intent intent = new Intent(getApplicationContext(), MainActivity2.class);
                        startActivity(intent);
                    }
                    else{
                        Log.e("postAutoLogin", "error code: " + response.code());
                        if(response.code() == 400)
                            printToast(iniSection.get("toast_not_entervalue"));
                        else if(response.code() == 500)
                            printToast(iniSection.get("toast_server_error"));
                    }
                }
                @Override
                public void onFailure(@NonNull Call<LoginResponse> call, @NonNull Throwable t){
                    Log.e("postAutoLogin", "에러 : " + t.getMessage());
                }
            });


        }
    }

}
