package com.example.myongji_pay.serivce;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

public class RestartService extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("Tag","RestartService is called"+intent.getAction());
        Intent i = new Intent(context,StartService.class);
        if(intent.getAction().equals("ACTION.RESTART.StartService")
                || intent.getAction().equals("android.intent.action.BOOT_COMPLETED")){
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                context.startForegroundService(i);
                Log.i("Tag", "startForegroundService");
            }else {
                context.startService(i);
                Log.i("Tag", "startService");
            }
        }
        /*
        서비스가 죽을때 알람으로 다시 서비스 등록
         */
//        if(intent.getAction().equals("ACTION.RESTART.StartService")){
//            Log.i("Tag", "ACTION.RESTART.StartService");
//            Intent i = new Intent(context,StartService.class);
//            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
//                context.startForegroundService(i);
//                Log.i("Tag", "startForegroundService");
//            }else{
//                context.startService(i);
//                Log.i("Tag", "startService");
//            }
//        }
//        /*
//        기기 재시작시 서비스 등록
//         */
//        if(intent.getAction().equals("android.intent.action.BOOT_COMPLETED")){
//            Log.i("Tag", "android.intent.action.BOOT_COMPLETED");
//            Intent i = new Intent(context,StartService.class);
//            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
//                context.startForegroundService(i);
//            }else{
//                context.startService(i);
//            }
//        }
    }
}
