package com.example.myongji_pay.serivce;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.myongji_pay.MainActivity;
import com.example.myongji_pay.R;

public class StartService extends Service {
    ServiceThread thread;
    Notification Notifi ;
    String ChannelID = "com.example.service.StartService";
    String ChannelName = "StartService";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public void onCreate() {
        unregisterRestartAlarm();
        super.onCreate();
        Log.d("Tag","onCreate() called");

    }
    //서비스가 시작되면 핸들러를 실행시킴
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("Tag","onStartCommand() called");
//        if(startId == 100) {
//            myServiceHandler handler = new myServiceHandler();
//            thread = new ServiceThread(handler);
//            thread.start();
//        }

        return START_STICKY;
    }
    //액티비티가 종료되면 registerRestartAlarm
    @Override
    public void onDestroy() {
        Log.i("Tag","StartService : onDestroyed called");
        registerRestartAlarm();
    }
    //액티비티가 종료되면 알람으로 서비스 재시작을 해줌
    private void registerRestartAlarm() {
        Log.i("Tag", "registerRestartAlarm");
        Intent intent = new Intent(StartService.this, RestartService.class);
        intent.setAction("ACTION.RESTART.StartService");
        PendingIntent sender = PendingIntent.getBroadcast(StartService.this, 0,intent,0);
        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
        //알람등록
        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,SystemClock.elapsedRealtime(),1000,sender);
    }
    //액티비티가 실행되면 알람을 꺼줌
    private void unregisterRestartAlarm() {
        Log.i("Tag", "스타트 서비스의 unregisterRestartAlarm");
        Intent intent = new Intent(StartService.this, RestartService.class);
        intent.setAction("ACTION.RESTART.StartService");
        PendingIntent sender = PendingIntent.getBroadcast(getApplicationContext(), 0,intent,0);
        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
        //알람취소
        alarmManager.cancel(sender);
    }

    class myServiceHandler extends Handler{
        @Override
        public void handleMessage(@NonNull Message msg) {
            Intent intent = new Intent(StartService.this, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(StartService.this, 0, intent,PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationChannel channel = new NotificationChannel(ChannelID,ChannelName,NotificationManager.IMPORTANCE_LOW);
            NotificationManager NM = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
            NM.createNotificationChannel(channel);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    Notifi = new Notification.Builder(getApplicationContext(), ChannelID)
                            .setContentTitle("서비스 실행")
                            .setContentText("백그라운드 서비스 동작")
                            .setSmallIcon(R.drawable.icon_detail)
                            .setTicker("알림")
                            .setContentIntent(pendingIntent)
                            .build();
                    Log.i("Tag", "notification");
                } else {
                    Notifi = new Notification.Builder(getApplicationContext(), ChannelID)
                            .setWhen(System.currentTimeMillis())
                            .build();
                }

            NM.notify(1,Notifi);
//        NM.cancel(startId);
//            startForeground(1,Notifi);
//            토스트 띄우기

        }
    };
}
