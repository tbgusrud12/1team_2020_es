package com.example.myongji_pay.ResponseData;

import com.google.gson.annotations.SerializedName;

public class CountResponse {
    @SerializedName("cardCount")
    private String card;
    @SerializedName("accountCount")
    private String account;
    @SerializedName("memCount")
    private String membership;

    public CountResponse(String card, String account, String membership) {
        this.card = card;
        this.account = account;
        this.membership = membership;
    }

    public String getCard() {
        return card;
    }

    public String getAccount() {
        return account;
    }

    public String getMembership() {
        return membership;
    }
}
