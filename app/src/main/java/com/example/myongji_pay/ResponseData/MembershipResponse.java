package com.example.myongji_pay.ResponseData;

import com.google.gson.annotations.SerializedName;

public class MembershipResponse {
    @SerializedName("_id")
    private String id;
    @SerializedName("membershipName")
    private String membershipName;
    @SerializedName("membershipNumber")
    private String membershipNumber;
    @SerializedName("membershipColor")
    private String membershipColor;
    @SerializedName("user")
    private User user;
    @SerializedName("_v")
    private String v;

    public MembershipResponse(String id, String membershipName, String membershipNumber, String membershipColor, User user, String v) {
        this.id = id;
        this.membershipName = membershipName;
        this.membershipNumber = membershipNumber;
        this.membershipColor = membershipColor;
        this.user = user;
        this.v = v;
    }

    //user 배열 값 받아오기 위한 클래스
    class User{
        @SerializedName("_id")
        private String id;
        @SerializedName("userId")
        private String userId;

        public User(String id, String userId) {
            this.id = id;
            this.userId = userId;
        }
    }

    public String getId() {
        return id;
    }

    public String getMembershipName() {
        return membershipName;
    }

    public String getMembershipNumber() {
        return membershipNumber;
    }

    public String getMembershipColor() {
        return membershipColor;
    }

    public User getUser() {
        return user;
    }
}