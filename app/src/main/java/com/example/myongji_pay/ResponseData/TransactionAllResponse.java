package com.example.myongji_pay.ResponseData;

import com.google.gson.annotations.SerializedName;

public class TransactionAllResponse {
    @SerializedName("bn")
    private String bankName;
    @SerializedName("td")
    private String tranDate;
    @SerializedName("tt")
    private String tranTime;
    @SerializedName("it")
    private String inoutType;
    @SerializedName("wpc")
    private String content;
    @SerializedName("tamt")
    private String amount;

    public TransactionAllResponse(String bankName, String tranDate, String tranTime, String inoutType, String content, String amount) {
        this.bankName = bankName;
        this.tranDate = tranDate;
        this.tranTime = tranTime;
        this.inoutType = inoutType;
        this.content = content;
        this.amount = amount;
    }

    public String getBankName() {
        return bankName;
    }

    public String getTranDate() {
        return tranDate;
    }

    public String getTranTime() {
        return tranTime;
    }

    public String getInoutType() {
        return inoutType;
    }

    public String getAmount() {
        return amount;
    }

    public String getContent() {
        return content;
    }
}
