package com.example.myongji_pay.ResponseData;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {
    @SerializedName("_id")
    private String id;
    @SerializedName("username")
    private String username;
    @SerializedName("userId")
    private String userId;
    @SerializedName("access_token")
    private String access_token;
    @SerializedName("refresh_token")
    private String refresh_token;
    @SerializedName("user_seq_no")
    private String user_seq_no;

    public LoginResponse(String id, String username, String userId, String access_token, String refresh_token, String user_seq_no) {
        this.id = id;
        this.username = username;
        this.userId = userId;
        this.access_token = access_token;
        this.refresh_token = refresh_token;
        this.user_seq_no = user_seq_no;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public String getUser_seq_no() {
        return user_seq_no;
    }

    public void setUser_seq_no(String user_seq_no) {
        this.user_seq_no = user_seq_no;
    }
}
