package com.example.myongji_pay.ResponseData;

import com.google.gson.annotations.SerializedName;


public class CardResponse {
    @SerializedName("_id")
    private String id;
    @SerializedName("bank")
    private String bank;
    @SerializedName("cardNumber_d")
    private String cardNumber_d;
    @SerializedName("cardColor")
    private String cardColor;
    @SerializedName("validity")
    private String validity;
    @SerializedName("user")
    private User user;
    @SerializedName("_v")
    private String v;

    public CardResponse(String id, String bank, String cardNumber_d, String cardColor,String validity, User user, String v) {
        this.id = id;
        this.bank = bank;
        this.cardNumber_d = cardNumber_d;
        this.cardColor = cardColor;
        this.validity = validity;
        this.user = user;
        this.v = v;
    }

    //user 배열 값 받아오기 위한 클래스
    class User{
        @SerializedName("_id")
        private String id;
        @SerializedName("userId")
        private String userId;

        public User(String id, String userId) {
            this.id = id;
            this.userId = userId;
        }
    }

    public String getCardColor() {
        return cardColor;
    }

    public String getId() {
        return id;
    }

    public String getCardNumber_d() {
        return cardNumber_d;
    }

    public String getValidity() {
        return validity;
    }

    public String getBank() {
        return bank;
    }


    public User getUser() {
        return user;
    }
}