package com.example.myongji_pay.ResponseData;

import com.google.gson.annotations.SerializedName;

public class RegisterResponse {
    @SerializedName("_id")
    private String id;
    @SerializedName("username")
    private String username;
    @SerializedName("userId")
    private String userId;
    @SerializedName("_v")
    private String v;

    public RegisterResponse(String id, String username, String userId, String v) {
        this.id = id;
        this.username = username;
        this.userId = userId;
        this.v = v;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
