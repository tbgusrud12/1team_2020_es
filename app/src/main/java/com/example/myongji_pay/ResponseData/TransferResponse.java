package com.example.myongji_pay.ResponseData;

import com.google.gson.annotations.SerializedName;

public class TransferResponse {
    @SerializedName("api_tran_dtm") // 거래일시
    private String api_tran_dtm;
    @SerializedName("account_holder_name") // 보낸사람
    private String account_holder_name;
    @SerializedName("dps_account_holder_name") // 받는 사람
    private String dps_account_holder_name;
    @SerializedName("dps_account_num_masked") // 뱓는 사람 계좌
    private String dps_account_num_masked;
    @SerializedName("dps_bank_name") // 받는 사람 은행
    private String dps_bank_name;
    @SerializedName("tran_amt") // 거래내역
    private String tran_amt;

    public TransferResponse(String api_tran_dtm, String account_holder_name, String dps_account_holder_name, String dps_account_num_masked, String dps_bank_name, String tran_amt) {
        this.api_tran_dtm = api_tran_dtm;
        this.account_holder_name = account_holder_name;
        this.dps_account_holder_name = dps_account_holder_name;
        this.dps_account_num_masked = dps_account_num_masked;
        this.dps_bank_name = dps_bank_name;
        this.tran_amt = tran_amt;
    }

    public String getAccount_holder_name() {
        return account_holder_name;
    }

    public String getDps_account_num_masked() {
        return dps_account_num_masked;
    }

    public String getDps_bank_name() {
        return dps_bank_name;
    }

    public String getDps_account_holder_name() {
        return dps_account_holder_name;
    }

    public String getTran_amt() {
        return tran_amt;
    }

    public String getApi_tran_dtm() {
        return api_tran_dtm;
    }
}
