package com.example.myongji_pay.ResponseData;

import com.google.gson.annotations.SerializedName;

public class AccountResponse {
    @SerializedName("fintech_use_num")
    private String fintechUseNum;
    @SerializedName("bank_name")
    private String bankName;
    @SerializedName("account_num_masked")
    private String accountNumMasked;

    public AccountResponse(String fintechUseNum, String bankName, String accountNumMasked) {
        this.fintechUseNum = fintechUseNum;
        this.bankName = bankName;
        this.accountNumMasked = accountNumMasked;
    }

    public String getFintechUseNum() {
        return fintechUseNum;
    }

    public String getBankName() {
        return bankName;
    }

    public String getAccountNumMasked() {
        return accountNumMasked;
    }
}
