package com.example.myongji_pay;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.example.myongji_pay.BottomMenu._Home;

import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;
public class SetPassWd extends Fragment {
    private View view;
    public static SetPassWd newInstance(){return new SetPassWd();}

    EditText pw[] = new EditText[6];
    Integer[] edit_id = {R.id.pw1,R.id.pw2,R.id.pw3,R.id.pw4,R.id.pw5,R.id.pw6};
    TextView textView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.drawer_auth_security_password, container, false);
        Toolbar sub_toolbar = (Toolbar)view.findViewById(R.id.sub_toolbar);
        ((MainActivity2)getActivity()).setSupportActionBar(sub_toolbar);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        //비밀번호 설정 페이지 ini파일 다국어 적용
        TextView title = (TextView)view.findViewById(R.id.secText);
        TextView password = (TextView)view.findViewById(R.id.password);
        TextView alert = (TextView)view.findViewById(R.id.passwd_text);
        Button btnNext = view.findViewById(R.id.btnNext);
        title.setText(iniSection.get("security"));
        password.setText(iniSection.get("pwd_hint"));
        alert.setText(iniSection.get("toast_new_pwd"));
        btnNext.setText(iniSection.get("cancellation"));

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity2)getActivity()).replaceFragment(_Home.newInstance(),null);
            }
        });
        for(int i = 0; i < 6; i++){
            pw[i] = (EditText)view.findViewById(edit_id[i]);
            pw[0].requestFocus();
            final int finalI = i;
            pw[i].addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) { }
                @Override
                public void afterTextChanged(Editable s) {
                    if (finalI < 5) {
                        pw[finalI + 1].requestFocus();
                    } else {
                        String passwd = pw[0].getText().toString() + pw[1].getText().toString() +
                                pw[2].getText().toString() + pw[3].getText().toString() + pw[4].getText().toString() + pw[5].getText().toString();
                        Bundle set_passWd = new Bundle();
                        set_passWd.putString("passwd", passwd);
                        ((MainActivity2)getActivity()).replaceFragment(SetPassWd_Check.newInstance(),set_passWd);
                    }
                }
            });
        }
        return view;
    }
}