package com.example.myongji_pay;

import com.example.myongji_pay.ResponseData.AccountResponse;
import com.example.myongji_pay.ResponseData.CardResponse;
import com.example.myongji_pay.ResponseData.CountResponse;
import com.example.myongji_pay.ResponseData.LoginResponse;
import com.example.myongji_pay.ResponseData.MembershipResponse;
import com.example.myongji_pay.ResponseData.RegisterResponse;
import com.example.myongji_pay.ResponseData.TransactionAllResponse;
import com.example.myongji_pay.ResponseData.TransferResponse;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitAPI {
    //회원가입
    @POST("/api/auth/register")
    Call<RegisterResponse> postRegister(@Body HashMap<String, String> params);
    //중복 ID 확인
    @POST("/api/auth/isExist")
    Call<String> postIdCheck(@Body HashMap<String, String> params);
    //로그인
    @POST("/api/auth/login")
    Call<LoginResponse> postLogIn(@Body HashMap<String, String> params);
    //자동로그인
    @POST("/api/auth/auto_login")
    Call<LoginResponse> postAutoLogIn();
    //로그아웃
    @POST("/api/auth/logout")
    Call<Void> postLogout();
    //회원탈퇴
    @DELETE("/api/auth/{id}")
    Call<Void> deleteUser(@Path("id") String id);
    //멤버십 추가
    @POST("api/memberships/")
    Call<MembershipResponse> postMembership(@Body HashMap<String, String> params);
    //멤버십 조회
    @GET("api/memberships/")
    Call<ArrayList<MembershipResponse>> getMemberships();
    //특정 멤버십 조회
    @GET("api/memberships/{id}")
    Call<MembershipResponse> getMembership();
    //특정 멤버 삭제
    @DELETE("api/memberships/{id}")
    Call<Void> deleteMembership(@Path("id") String id);
    // 비밀번호 및 앱비밀번호 수정
    @PATCH("/api/auth/register/")
    Call<RegisterResponse> patch_pwd(@Body HashMap<String, String> params);
    //앱비밀번호 check
    @POST("/api/auth/checkapppwd")
    Call<String> postPwdCheck(@Body HashMap<String, String> params);
    //사용자의 비밀번호 check
    @POST("/api/auth/checkpwd")
    Call<String> postpwdCheck(@Body HashMap<String, String> params);
    //카드 추가
    @POST("api/cards/")
    Call<CardResponse> postCard(@Body HashMap<String, String> params);
    // 전체 카드 조회
    @GET("api/cards/")
    Call<ArrayList<CardResponse>> getCards();
    //특정 카드 조회
    @GET("api/cards/{id}")
    Call<CardResponse> getCard();
    //특정 카드 삭제
    @DELETE("api/cards/{id}")
    Call<Void> deleteCard(@Path("id") String id);

    //사용자 모든 계좌 조회
    @GET("api/account/list")
    Call<ArrayList<AccountResponse>> getAccountList();
    //사용자 모든 계좌 조회
    @GET("api/account/balance/{finUseNum}")
    Call<String> getAccountBalance(@Path("finUseNum") String finUseNum);
    //통합내역조회
    @GET("api/account/transaction_all")
    Call<ArrayList<TransactionAllResponse>> getTransactionAll(
            @Query("from_date") String fromDate,
            @Query("to_date") String toDate);
    //통합내역조회(그래프용)
    @GET("api/account/transaction_graph")
    Call<ArrayList<TransactionAllResponse>> getTransactionGraph(
            @Query("from_date") String fromDate,
            @Query("to_date") String toDate);

    @POST("api/account/transfer")
    Call<TransferResponse> postTransfer(@Body HashMap<String, String> params);

    //카드, 계좌, 멤버십 갯수 조회
    @GET("api/auth/count")
    Call<CountResponse> getCount();
}

