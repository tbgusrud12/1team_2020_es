package com.example.myongji_pay;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;
import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;
public class AddCard extends Fragment {
    Button camera_btn;
    Button next_btn;
    EditText cvc, pw1, pw2, month, year;
    EditText num[] = new EditText[4];
    EditText pw[] = new EditText[2];
    EditText Valid_date[] = new EditText[2];
    Integer[] num_id = {R.id.cardnum1,R.id.cardnum2,R.id.cardnum3,R.id.cardnum4};
    Integer[] pw_id = {R.id.pw1,R.id.pw2};
    Integer[] valid_date_id = {R.id.month, R.id.year};
    RadioGroup radioGroup;
    String color;
    TextView card_num, card_pwd, card_color, valid_date, add_card;
    int MY_SCAN_REQUEST_CODE;
    public static AddCard newInstance(){ return new AddCard();}
    private View view;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.add_card,container,false);
        //상단 툴바 설정
        Toolbar sub_toolbar = (Toolbar)view.findViewById(R.id.sub_toolbar);
        ((MainActivity2)getActivity()).setSupportActionBar(sub_toolbar);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        //카드 추가 페이지 ini파일 다국어 설정
        //TextView
        add_card=view.findViewById(R.id.secText);
        card_num=view.findViewById(R.id.card_num);
        valid_date=view.findViewById(R.id.valid_date);
        card_pwd=view.findViewById(R.id.card_pwd);
        card_color=view.findViewById(R.id.card_color);
        //Button
        camera_btn=view.findViewById(R.id.camera_btn);
        next_btn=view.findViewById(R.id.next);
        //EditText
        month=view.findViewById(R.id.month);
        year=view.findViewById(R.id.year);
        cvc = view.findViewById(R.id.cvc);
        pw1 = view.findViewById(R.id.pw1);
        pw2= view.findViewById(R.id.pw2);
        //다국어 설정
        add_card.setText(iniSection.get("add_card"));
        card_num.setText(iniSection.get("card_number"));
        valid_date.setText(iniSection.get("validity"));
        card_pwd.setText(iniSection.get("card_passward"));
        card_color.setText(iniSection.get("card_color"));
        cvc.setHint(iniSection.get("three_digit"));
        next_btn.setText(iniSection.get("next"));

        radioGroup = view.findViewById(R.id.cardColorRadioGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.btn_black:
                        color = "black";
                        break;
                    case R.id.btn_red:
                        color = "red";
                        break;
                    case R.id.btn_orange:
                        color = "orange";
                        break;
                    case R.id.btn_yellow:
                        color = "yellow";
                        break;
                    case R.id.btn_green:
                        color = "green";
                        break;
                    case R.id.btn_navy:
                        color = "navy";
                        break;
                    case R.id.btn_purple:
                        color = "purple";
                        break;
                    default:
                        color = null;
                }
            }
        });

//        String card_num = num1.getText().toString() + "-" + num2.getText().toString();
        camera_btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                onScanPress();
            }
        });
        for(int i = 0; i < 4; i++){
            Log.d("num_Tag", i+"번째 입력");
            num[i] = (EditText)view.findViewById(num_id[i]);
            num[0].requestFocus();
            final int finalI = i;
//            num[i].setSaveFromParentEnabled(false);
//            num[i].setSaveEnabled(true);
            num[i].addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {}
                @Override
                public void afterTextChanged(Editable s) {
                    if(finalI < 3 && num[finalI].getText().toString().length() > 3){
                        num[finalI+1].requestFocus();
                    }else if(finalI == 3 && num[finalI].getText().toString().length() > 3){
                        Valid_date[0].requestFocus();
                    }
                }
            });
        }
        for(int i = 0; i < 2; i++){
            pw[i] = (EditText)view.findViewById(pw_id[i]);
            Log.d("num_Tag", i+"번째 입력");
            Valid_date[i] = (EditText)view.findViewById(valid_date_id[i]);
            final int finalI = i;
            Valid_date[i].addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after){ }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count){ }
                @Override
                public void afterTextChanged(Editable s) {
                    if(finalI < 1 && Valid_date[finalI].getText().toString().length() > 1){
                        Valid_date[finalI+1].requestFocus();
                    }else if(finalI == 1 && Valid_date[finalI].getText().toString().length() > 1){
                       cvc.requestFocus();
                    }
                }
            });
            pw[i].addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {}
                @Override
                public void afterTextChanged(Editable s) {
                    if(finalI < 1){
                        pw[finalI+1].requestFocus();
                    }
                }
            });
        }
        cvc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override
            public void afterTextChanged(Editable s) {
                if(cvc.getText().toString().length() > 2){
                    pw[0].requestFocus();
                }
            }
        });
        next_btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if (num[0].getText().toString().replace(" ", "").equals("")){
                    printToast(iniSection.get("toast_check_card_num"));
                    num[0].requestFocus();
                    showKeypad();
                }
                else if (num[1].getText().toString().replace(" ", "").equals("")){
                    printToast(iniSection.get("toast_check_card_num"));
                    num[1].requestFocus();
                    showKeypad();
                }
                else if (num[2].getText().toString().replace(" ", "").equals("")){
                    printToast(iniSection.get("toast_check_card_num"));
                    num[2].requestFocus();
                    showKeypad();
                }
                else if (num[3].getText().toString().replace(" ", "").equals("")){
                    printToast(iniSection.get("toast_check_card_num"));
                    num[3].requestFocus();
                    showKeypad();
                }
                else if (month.getText().toString().replace(" ", "").equals("")){
                    printToast(iniSection.get("toast_check_validate"));
                    month.requestFocus();
                    showKeypad();
                }
                else if (year.getText().toString().replace(" ", "").equals("")){
                    printToast(iniSection.get("toast_check_validate"));
                    year.requestFocus();
                    showKeypad();
                }
                else if (cvc.getText().toString().replace(" ", "").equals("")){
                    printToast(iniSection.get("toast_check_CVC"));
                    cvc.requestFocus();
                    showKeypad();
                }
                else if (pw1.getText().toString().replace(" ", "").equals("")){
                    printToast(iniSection.get("toast_check_card_pwd"));
                    pw1.requestFocus();
                    showKeypad();
                }
                else if(color == null){
                    printToast(iniSection.get("toast_selet_cardcolor"));
                }
                else {
                    String cardNumber_d = num[0].getText().toString();
                    String cardNumber =num[1].getText().toString()+num[2].getText().toString()+num[3].getText().toString();
                    String validity =month.getText().toString()+year.getText().toString();
                    String cardCvc = cvc.getText().toString();
                    String cardPassword =pw1.getText().toString()+pw2.getText().toString();
                    String bank = getBank(cardNumber_d + cardNumber.substring(0,2));
                    Bundle card = new Bundle();
                    card.putString("bank", bank);
                    card.putString("cardNumber_d",cardNumber_d);
                    card.putString("cardNumber", cardNumber);
                    card.putString("validity", validity);
                    card.putString("cardCvc", cardCvc);
                    card.putString("cardPassword", cardPassword);
                    card.putString("cardColor", color);
                    ((MainActivity2)getActivity()).replaceFragment(AddCardSign.newInstance(),card);
                }
            }
        });
        return view;
    }
//    @Override
    private void onScanPress(){
        Intent scanIntent = new Intent(getActivity(), CardIOActivity.class);

        // customize these values to suit your needs.
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, false); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false

        // MY_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.
        startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_SCAN_REQUEST_CODE) {
            String resultDisplayStr;
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

                // Never log a raw card number. Avoid displaying it, but if necessary use getFormattedCardNumber()
                resultDisplayStr = "Card Number: " + scanResult.getRedactedCardNumber() + "\n";

                // Do something with the raw number, e.g.:
                // myService.setCardNumber( scanResult.cardNumber );
                num[0].setText(scanResult.cardNumber.substring(0,4));
                num[1].setText(scanResult.cardNumber.substring(4,8));
                num[2].setText(scanResult.cardNumber.substring(8,12));
                num[3].setText(scanResult.cardNumber.substring(12,16));


                if (scanResult.isExpiryValid()) {
                    resultDisplayStr += "Expiration Date: " + scanResult.expiryMonth + "/" + scanResult.expiryYear + "\n";
                    if(scanResult.expiryMonth < 10)
                        Valid_date[0].setText("0" + Integer.toString(scanResult.expiryMonth));
                    else
                        Valid_date[0].setText(Integer.toString(scanResult.expiryMonth));
                    Valid_date[1].setText(Integer.toString(scanResult.expiryYear).substring(2,4));
                }

                if (scanResult.cvv != null) {
                    // Never log or display a CVV
                    resultDisplayStr += "CVV has " + scanResult.cvv.length() + " digits.\n";
                }

                if (scanResult.postalCode != null) {
                    resultDisplayStr += "Postal Code: " + scanResult.postalCode + "\n";
                }
            }
            else {
                resultDisplayStr = "Scan was canceled.";
            }
            // do something with resultDisplayStr, maybe display it in a textView
            //cardnum1.setText(resultDisplayStr);
        }

        // else handle other activity results
    }
    public void showKeypad(){
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public void printToast(String data){
        Toast.makeText(getContext(), data, Toast.LENGTH_SHORT).show();
    }

    //카드번호로 은행사 조회하기
    String getBank(String cardnum){
        String bank;
        Log.d("1", cardnum);
        switch(cardnum){
            case "510737":
                bank = "신한체크카드";
                break;
            case "625840":
                bank = "신한 Super KT 카드";
            case "557042":
                bank = "KB국민카드";
                break;
            case "536510":
                bank = "카카오뱅크 체크카드";
                break;
            case "625006":
                bank = "KB국민카드";
                break;
            default:
                bank = "카드";
        }
        return bank;
    }
}
