package com.example.myongji_pay;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import org.w3c.dom.Text;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.myongji_pay.RetrofitClient.getApiService;
import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;

public class Signout extends AppCompatActivity {
    private boolean success = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_out);

        Button yes = (Button) findViewById(R.id.sign_out_yes);
        Button no = (Button) findViewById(R.id.sign_out_no);
        TextView title = (TextView)findViewById(R.id.really);
        yes.setText(iniSection.get("yes"));
        no.setText(iniSection.get("no"));
        title.setText(iniSection.get("right_leave"));

        yes.setOnClickListener(new View.OnClickListener(){  //확인 버튼 클릭시...
            public void onClick(View v){
                deleteUserRequest();

            }
        });

        no.setOnClickListener(new View.OnClickListener(){   //취소 버튼 클릭 시 내정보 화면으로 이동
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), MainActivity2.class);
                Toast.makeText(Signout.this.getApplicationContext(), iniSection.get("toast_cancel"), Toast.LENGTH_SHORT).show();
                startActivity(intent);
            }
        });
    }
    public void deleteUserRequest(){
        Call<Void> call = getApiService(this).deleteUser(User.id);
        call.enqueue(new Callback<Void>(){
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.isSuccessful()){
                    success = true;
                    Toast.makeText(Signout.this.getApplicationContext(), iniSection.get("toast_mem_withdrawel"), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplication(), MainActivity.class);
                    startActivity(intent);
                }
                else{
                    if(response.code()==500)
                        printToast(iniSection.get("toast_server_error"));
                    success = false;
                }

            }
            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t){
                success = false;
            }
        });
    }
    public void printToast(String data){
        Toast.makeText(this, data, Toast.LENGTH_SHORT).show();
    }

}
