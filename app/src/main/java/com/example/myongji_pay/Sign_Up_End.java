package com.example.myongji_pay;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;
public class Sign_Up_End extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_end);
        Button login = (Button)findViewById(R.id.login);
        TextView title = (TextView)findViewById(R.id.title);
        login.setText(iniSection.get("login"));
        title.setText(iniSection.get("sign_up_complete"));
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
