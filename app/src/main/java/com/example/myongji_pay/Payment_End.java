package com.example.myongji_pay;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;

public class Payment_End extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_end);
        TextView title = (TextView)findViewById(R.id.title);
        TextView used = (TextView)findViewById(R.id.used);
        TextView card = (TextView)findViewById(R.id.card);
        TextView date = (TextView)findViewById(R.id.date);
        TextView money = (TextView)findViewById(R.id.money);
        Button btnNext = (Button)findViewById(R.id.next);

        title.setText(iniSection.get("payment_completed"));
        used.setText(iniSection.get("myongi_cafe"));
        card.setText(iniSection.get("payment_card"));
        date.setText(iniSection.get("payment_date"));
        money.setText(iniSection.get("payment_amount"));
        btnNext.setText(iniSection.get("check"));

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),MainActivity2.class);
                startActivity(intent);
            }
        });

    }
}
