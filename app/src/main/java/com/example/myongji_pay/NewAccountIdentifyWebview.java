package com.example.myongji_pay;

import android.os.Bundle;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class NewAccountIdentifyWebview extends AppCompatActivity {
    private WebView wv;
    private WebSettings wvSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.identify_webview);

        wv = findViewById(R.id.identify_webView);
        wv.setWebViewClient(new WebViewClient());
        wvSettings = wv.getSettings();
        wvSettings.setJavaScriptEnabled(true);
        wv.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url){
                String uri = view.getUrl();
                Log.d("###Finished### : ",uri);
                if(uri.startsWith("http://10.0.2.2:4000/")){
                        finish();



                }


            }
        });

        wv.loadUrl("https://testapi.openbanking.or.kr/oauth/2.0/authorize?response_type=code&client_id=EsOL6RK1exea8gMpXtVhKjDoEW7mf6aYsw7fcwvu&redirect_uri=http://10.0.2.2:4000/api/auth/refreshtoken/&scope=login inquiry transfer&state=12345678901234567890123456789012&auth_type=0");
    }

    @Override
    public void onBackPressed(){
        //뒤로가기 버튼 클릭 시 이전 웹페이지 이동. 이전 웹페이지가 없다면 이전 layout
        if(wv.canGoBack()){
            wv.goBack();
        }else{
            super.onBackPressed();
        }
    }

        public void printToast(String data){
            Toast.makeText(this.getApplicationContext(), data, Toast.LENGTH_SHORT).show();
        }
}




