package com.example.myongji_pay;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.myongji_pay.BottomMenu._Home;
import com.example.myongji_pay.BottomMenu._MyCard;
import com.example.myongji_pay.BottomMenu._Payment;
import com.example.myongji_pay.BottomMenu._Send;
import com.example.myongji_pay.DrawerMenu.Drawer_Auth_Security;
import com.example.myongji_pay.DrawerMenu.Drawer_Details_check;
import com.example.myongji_pay.DrawerMenu.Drawer_Language;
import com.example.myongji_pay.DrawerMenu.Drawer_Membership;
import com.example.myongji_pay.DrawerMenu.Drawer_MyPage;
import com.example.myongji_pay.serivce.RestartService;
import com.example.myongji_pay.serivce.StartService;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import java.util.Stack;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.myongji_pay.RetrofitClient.getApiService;
import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;
//ini 파일을 받아오기 위한 변수

public class MainActivity2 extends AppCompatActivity{
    private boolean success = false;
    private DrawerLayout mDrawerLayout;
    private Context context = this;
    public static Stack<Fragment>fragmentStack;
    ActionBarDrawerToggle drawerToggle;
    BottomNavigationView bottomNavigationView;
    NavigationView navigationView;
    public static FragmentManager fm;
    public static FragmentTransaction ft;
    _Home home = new _Home();
    _MyCard mycard = new _MyCard();
    _Payment payment = new _Payment();
    _Send send = new _Send();
    Drawer_MyPage mypage = new Drawer_MyPage();
    Drawer_Language language = new Drawer_Language();
    Drawer_Auth_Security security = new Drawer_Auth_Security();
    Drawer_Membership membership = new Drawer_Membership();
    Drawer_Details_check details_check = new Drawer_Details_check();
    private RestartService restartService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_2);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        //토글버튼 객체 생성
        drawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar,R.string.app_name, R.string.app_name);
        drawerToggle.syncState(); // 삼선 메뉴 만들기
        mDrawerLayout.addDrawerListener(drawerToggle);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //기본 화면을 홈 화면으로 해줌
        fm = getSupportFragmentManager();
        ft = fm.beginTransaction();
        ft.add(R.id.main_frame,home);
        ft.commit();
        //네이게이션 변수들 할당
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav);
        //드로어 메뉴 헤더에 접근해서 유저 이름을 바꿔줌
        View header_view = navigationView.getHeaderView(0);
        TextView userName = (TextView)header_view.findViewById(R.id.User_name);

        //드로어 메뉴 메뉴에 접근해서 메뉴 아이템의 이름을 바꿔줌
        Menu nav_menu = navigationView.getMenu();
        MenuItem nav_myPage = (MenuItem) nav_menu.findItem(R.id.account);
        MenuItem nav_security = (MenuItem) nav_menu.findItem(R.id.security);
        MenuItem nav_language = (MenuItem) nav_menu.findItem(R.id.language);
        MenuItem nav_membership = (MenuItem) nav_menu.findItem(R.id.membership);
        MenuItem nav_details = (MenuItem) nav_menu.findItem(R.id.details_check);
        //네비게이션 타이틀 설정
        nav_myPage.setTitle(iniSection.get("info"));
        nav_security.setTitle(iniSection.get("security"));
        nav_language.setTitle(iniSection.get("lan"));
        nav_membership.setTitle(iniSection.get("member"));
        nav_details.setTitle(iniSection.get("details_check"));

        //바텀네비게이션 메뉴에 접근해서 메뉴 아이템의 이름을 바꿔줌
        Menu bottom_menu = bottomNavigationView.getMenu();
        MenuItem bottom_home = (MenuItem)bottom_menu.findItem(R.id.Home);
        MenuItem bottom_mycard = (MenuItem)bottom_menu.findItem(R.id.mycard);
        MenuItem bottom_payment = (MenuItem)bottom_menu.findItem(R.id.Payment);
        MenuItem bottom_send = (MenuItem)bottom_menu.findItem(R.id.Send);
        //바텀네비게이션 타이틀 설정
        bottom_home.setTitle(iniSection.get("title_home"));
        bottom_mycard.setTitle(iniSection.get("title_MyCard"));
        bottom_payment.setTitle(iniSection.get("title_Payment"));
        bottom_send.setTitle(iniSection.get("title_Send"));

        //로그아웃 버튼
        Button logout = (Button)header_view.findViewById(R.id.logout);
        logout.setText(iniSection.get("logout"));
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logOutRequest();
            }
        });
        userName.setText(User.userName);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                item.setChecked(true);
                mDrawerLayout.closeDrawers();
                fm = getSupportFragmentManager();
                ft = fm.beginTransaction();
                switch (item.getItemId()) {
                    case R.id.account:
                        ft.replace(R.id.main_frame, mypage);
                        break;
                    case R.id.security:
                        ft.replace(R.id.main_frame, security);
                        break;
                    case R.id.language:
                        ft.replace(R.id.main_frame, language);
                        break;
                    case R.id.membership:
                        ft.replace(R.id.main_frame, membership);
                        break;
                    case R.id.details_check:
                        ft.replace(R.id.main_frame, details_check);
                        break;
                    default:
                        break;
                }
                ft.addToBackStack(null);
                ft.commit();
                return true;
            }
        });
        //바텀 네비게이션 구현

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                fm = getSupportFragmentManager();
                ft = fm.beginTransaction();
                fm.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
                switch(item.getItemId()){
                    case R.id.Home:
                        ft.replace(R.id.main_frame,home,"Home");
                        break;
                    case R.id.mycard:
                        ft.replace(R.id.main_frame,mycard, "MyCard");
                        break;
                    case R.id.Payment:
                        ft.replace(R.id.main_frame,payment,"Payment");
                        break;
                    case R.id.Send:
                        ft.replace(R.id.main_frame,send,"Send");
                        break;
                    default:
                        break;
                }
                ft.addToBackStack(null);
                ft.commit();
                return true;
            }
        });
        initData();
    }
    public void initData(){
        restartService = new RestartService();
        Intent intent = new Intent(MainActivity2.this, StartService.class);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.example.myongji_pay.serivce.StartService");
        //브로드 캐스트 등록
        registerReceiver(restartService,intentFilter);
        Log.i("Tag","브로드캐스트 등록"+ intentFilter);
        //서비스 시작
        startService(intent);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("Tag", "onDestroy Called");
        unregisterReceiver(restartService);
        Intent intent = new Intent(MainActivity2.this, StartService.class);
        stopService(intent);
    }

    //프래그먼트 끼리의 화면 전환을 함
    public void replaceFragment(Fragment fragment, Bundle msg){
        fragment.setArguments(msg);
        fm = getSupportFragmentManager();
        ft = fm.beginTransaction();
        ft.replace(R.id.main_frame,fragment);
        ft.addToBackStack(null);
        ft.commit();
    }
    public void refreshFragment(Fragment fragment){
        Log.d("new_Tag", String.valueOf(fragment));
        fm = getSupportFragmentManager();
        ft = fm.beginTransaction();
        ft.detach(fragment).attach(fragment).commit();
    }
    //프래그먼트에서 뒤로가기 버튼을 눌렀을 때 실행
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:{
                onBackPressed();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
    public void logOutRequest(){
        Call<Void> call = getApiService(this).postLogout();
        call.enqueue(new Callback<Void>(){
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.isSuccessful()){
                    success = true;
                    SharedPreferences sf = getSharedPreferences("permission", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sf.edit();
                    editor.clear();
                    editor.commit();
                    Toast.makeText(MainActivity2.this.getApplicationContext(), iniSection.get("toast_logout"), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplication(), MainActivity.class);
                    startActivity(intent);
                }
                else{
                    if(response.code()==500)
                        printToast(iniSection.get("toast_server_error"));
                    success = false;
                }

            }
            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t){
                success = false;
            }
        });
    }
    public void printToast(String data){
        Toast.makeText(this, data, Toast.LENGTH_SHORT).show();
    }

}
