package com.example.myongji_pay;

public class TransactionData extends TransactionDetailAdapterItem {
    String title;
    String amount;
    String p_time;
    String bank;
    String type;



    public TransactionData(String title, String amount, String p_time, String bank, String type, int year, int month, int dayOfMonth) {
        super(year, month, dayOfMonth);
        this.title = title;
        this.amount=amount;
        this.p_time=p_time;
        this.bank=bank;
        this.type=type;
    }

    @Override
    public int getType() {
        return TYPE_DATA;
    }

    public String getTitle() {
        return title;
    }
    public String getAmount(){
        return amount;
    }
    public String getPTime(){
        return p_time;
    }
    public String getBank(){
        return bank;
    }
    /*송금 or 결제*/
    public String getType_(){
        return type;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public void setAmount(String amount){
        this.amount=amount;
    }
    public void setTime(String time){
        this.p_time=time;
    }
    public void setBank(String bank){
        this.bank=bank;
    }
    /*송금 or 결제*/
    public void setType(String type){
        this.type=type;
    }
}

