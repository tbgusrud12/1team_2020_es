package com.example.myongji_pay;

// 회원가입 Page => MainActivity -> SignupActivity (signup.xml)

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myongji_pay.ResponseData.RegisterResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.myongji_pay.RetrofitClient.*;
import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;

public class SignupActivity extends AppCompatActivity {
    private Button identify, idCheck, signup;
    private EditText name, id, pw, pwCheck;
    private CheckBox ck1, ck2;

    private boolean idChecked;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        //Button
        identify = (Button)findViewById(R.id.signup_identify);
        idCheck = (Button)findViewById(R.id.signup_idCheck);
        signup = (Button)findViewById(R.id.signup_signUP);
        //EditText
        name = (EditText)findViewById(R.id.signup_name);
        id = (EditText)findViewById(R.id.signup_id);
        pw = (EditText)findViewById(R.id.signup_password);
        pwCheck = (EditText)findViewById(R.id.signup_passwordcheck);
        //CheckBox
        ck1 = (CheckBox)findViewById(R.id.signup_ck1);
        ck2 = (CheckBox)findViewById(R.id.signup_ck2);
        //회원가입 페이지 ini파일 다국어 적용
        identify.setText(iniSection.get("identity_sertification"));
        idCheck.setText(iniSection.get("duplication_check"));
        signup.setText(iniSection.get("signup"));
        name.setHint(iniSection.get("name_hint"));
        pwCheck.setHint(iniSection.get("pwd_conf"));
        ck1.setText(iniSection.get("consent_info"));
        ck2.setText(iniSection.get("age_order"));
        id.setHint(iniSection.get("id_hint"));
        pw.setHint(iniSection.get("pwd_hint2"));
        idChecked = false;

        identify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               AlertDialog.Builder builder = new AlertDialog.Builder(SignupActivity.this);
               builder.setTitle(iniSection.get("toast_self_auth"));
               builder.setPositiveButton(iniSection.get("check"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getApplicationContext(), SignupIdentifyWebview.class);
                        startActivity(intent);
                    }
                });
               builder.show();
            }
        });
        //id 중복 확인
        idCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //id 빈칸일 경우 빈칸임을 알려준 후 포커스. 값 입력 되었을 경우 서버로 request
                if(id.getText().toString().replace(" ", "").equals("")){
                    printToast(iniSection.get("toast_enter_id"));
                    id.requestFocus();
                    showKeypad();
                }
                else idCheckRequest();
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //이름 입력 안했으면 이름 edittext에 focus 후 키보드 띄움
                if(name.getText().toString().replace(" ", "").equals("")){
                    printToast(iniSection.get("toast_enter_name"));
                    name.requestFocus();
                    showKeypad();
                }
                //비밀번호 입력 안했으면 pw edittext에 focus 후 키보드 띄움
                else if(pw.getText().toString().replace(" ", "").equals("")){
                    printToast(iniSection.get("toast_enter_pwd"));
                    pw.requestFocus();
                    showKeypad();
                }
                //비밀번호 확인 안했으면 pwCheck edittext에 focus 후 키보드 띄움
                else if(pwCheck.getText().toString().replace(" ", "").equals("")){
                    printToast(iniSection.get("toast_enter_pwd2"));
                    pwCheck.requestFocus();
                    showKeypad();
                }
                //비밀번호, 비밀번호 확인 일치 여부 확인
                else if (!pw.getText().toString().equals(pwCheck.getText().toString())){
                    printToast(iniSection.get("toast_pwd_not_match"));
                    pw.setText(null);
                    pwCheck.setText(null);
                    pw.requestFocus();
                    showKeypad();
                }
                    //ID 중복체크 여부 확인
                else if(idChecked == false){
                    printToast(iniSection.get("toast_id_duplicate"));
                }
                //본인인증 여부 확인
                else if(Identified.identified == false)
                    printToast(iniSection.get("toast_auth_yourself"));
                //체크박스 모두 확인 했는지 확인후 했으면 서버에 회원가입 요청
                else if(!(ck1.isChecked() && ck2.isChecked())){
                    printToast(iniSection.get("toast_agree"));
                }
                else
                    registerRequest();
            }
        });
    }

    //서버에 입력된 정보로 회원가입 request
    public void registerRequest() {
        //params에 입력 데이터 삽입
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("username", name.getText().toString());
        params.put("userId", id.getText().toString());
        params.put("password", pw.getText().toString());

        //POST 요청
        Call<RegisterResponse> call = getApiService(this).postRegister(params);
        call.enqueue(new Callback<RegisterResponse>(){
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                if(response.isSuccessful()){
                    Log.d("postRegister", "success POST");
                    //가입 완료 후 identified 전역변수 false
                    Identified.identified = false;
                    //회원가입 완료 창으로 이동
                    Intent intent = new Intent(getApplicationContext(), Sign_Up_End.class);
                    startActivity(intent);
                }
                else{
                    Log.e("postRegister", "error code: " + response.code());
                    if(response.code() == 400)
                        printToast(iniSection.get("toast_not_entervalue"));
                    else if(response.code() == 500)
                        printToast(iniSection.get("toast_server_error"));
                }

            }
            @Override
            public void onFailure(@NonNull Call<RegisterResponse> call, @NonNull Throwable t){
                Log.e("postRegister", "에러 : " + t.getMessage());
            }
        });

    }

    //서버에 중복 ID 존재 여부 request
    public void idCheckRequest(){
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("userId", id.getText().toString());
        Call<String> call = getApiService(this).postIdCheck(params);
        call.enqueue(new Callback<String>(){
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    idChecked = true;
                    printToast(iniSection.get("toast_available_id"));
                    Log.d("idCheck", "success POST");
                }
                else{
                    if(response.code()==409) {
                        printToast(iniSection.get("toast_exist_id"));
                        id.setText(null);
                        id.requestFocus();
                        showKeypad();

                    }
                    idChecked = false;
                    Log.e("idCheck", "error code: " + response.code());
                }

            }
            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t){
                idChecked = false;
                Log.e("idCheck", "에러 : " + t.getMessage());
            }
        });
    }

    public void showKeypad(){
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public void printToast(String data){
        Toast.makeText(this.getApplicationContext(), data, Toast.LENGTH_SHORT).show();
    }
}



