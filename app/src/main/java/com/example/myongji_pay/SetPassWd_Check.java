package com.example.myongji_pay;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.example.myongji_pay.BottomMenu._Home;
import com.example.myongji_pay.ResponseData.RegisterResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.myongji_pay.RetrofitClient.getApiService;
import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;
public class SetPassWd_Check extends Fragment {
    public static SetPassWd_Check newInstance(){return new SetPassWd_Check();}
    EditText pw[] = new EditText[6];
    Integer[] edit_id = {R.id.pw1,R.id.pw2,R.id.pw3,R.id.pw4,R.id.pw5,R.id.pw6};
    private String passwd;
    private String input_passwd;
    private boolean success = false;
    String inserted_passwd;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.drawer_auth_security_password_check, container, false);
        Toolbar sub_toolbar = (Toolbar)view.findViewById(R.id.sub_toolbar);
        ((MainActivity2)getActivity()).setSupportActionBar(sub_toolbar);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        //비밀번호 설정 페이지 ini파일 다국어 적용
        TextView title = (TextView)view.findViewById(R.id.secText);
        TextView password = (TextView)view.findViewById(R.id.password);
        TextView alert = (TextView)view.findViewById(R.id.passwd_text);
        Button btnNext = view.findViewById(R.id.btnNext);
        title.setText(iniSection.get("security"));
        password.setText(iniSection.get("pwd_conf"));
        alert.setText(iniSection.get("enter_passward_agin"));
        btnNext.setText(iniSection.get("check"));
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(success) {
                    ((MainActivity2) getActivity()).replaceFragment(_Home.newInstance(), null);
                }
                else{
                    printToast(iniSection.get("toast_new_pwd"));
                }
            }
        });
        for(int i = 0; i < 6; i++){
            pw[i] = (EditText)view.findViewById(edit_id[i]);
            pw[0].requestFocus();
            final int finalI = i;
            pw[i].addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {}
                @Override
                public void afterTextChanged(Editable s) {
                    if(finalI < 5) {
                        pw[finalI + 1].requestFocus();
                    }else {
                        input_passwd = pw[0].getText().toString()+pw[1].getText().toString()+pw[2].getText().toString()+
                                pw[3].getText().toString()+pw[4].getText().toString()+pw[5].getText().toString();
                        if(input_passwd.equals(inserted_passwd)){
                            appPwdRequest();

                        }else{
                            Toast.makeText(getActivity(),iniSection.get("toast_pwd_not_match"), Toast.LENGTH_SHORT).show();
                            for(int i = 0; i < 6; i++){
                                pw[i].getText().clear();
                                pw[0].requestFocus();
                            }
                        }
                    }
                }
            });
        }

        return view;
    }
    // 서버에 입력된 앱비밀번호 수정 request
    public void appPwdRequest(){
        //params에 입력데이터 삽입
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("AppPwd", input_passwd);

        // PATCH 요청
        Call<RegisterResponse> call = getApiService(getActivity()).patch_pwd(params);
        call.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                if(response.isSuccessful()){
                    Log.d("patch_pwd", "success PATCH");
                    success = true;
                  Toast.makeText(getActivity(),iniSection.get("toast_pwd_change"), Toast.LENGTH_SHORT).show();
                    ((MainActivity2) getActivity()).replaceFragment(_Home.newInstance(), null);
                }
                else{
                    Log.e("patch_pwd", "error code: " + response.code());
                    if(response.code() == 400)
                        printToast(iniSection.get("toast_not_entervalue"));
                    else if(response.code() == 500)
                        printToast(iniSection.get("toast_server_error"));
                }
            }

            @Override
            public void onFailure(@NonNull Call<RegisterResponse> call, @NonNull Throwable t){
                Log.e("patch_pwd", "에러 : " + t.getMessage());
            }
        });
    }
    public void printToast(String data){
        Toast.makeText(this.getActivity(), data, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStart() {
        super.onStart();
        if(getArguments() != null) {
            Bundle passWd = this.getArguments();
            inserted_passwd = passWd.getString("passwd");
        }
    }
}
