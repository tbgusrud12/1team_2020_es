package com.example.myongji_pay;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.example.myongji_pay.BottomMenu._Home;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;
import static com.example.myongji_pay.RetrofitClient.getApiService;
public class SetPassWd_Change extends Fragment {
    public static SetPassWd_Change newInstance(){return new SetPassWd_Change();}
    EditText pw[] = new EditText[6];
    private boolean success;
    String pwd;

    Integer[] edit_id = {R.id.pw1,R.id.pw2,R.id.pw3,R.id.pw4,R.id.pw5,R.id.pw6};
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.drawer_auth_security_password_change, container, false);
        Toolbar sub_toolbar = (Toolbar)view.findViewById(R.id.sub_toolbar);
        ((MainActivity2)getActivity()).setSupportActionBar(sub_toolbar);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity2)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        //패스워드 설정 페이지 ini파일 다국어 적용
        TextView title = (TextView)view.findViewById(R.id.secText);
        TextView password = (TextView)view.findViewById(R.id.password);
        TextView alert = (TextView)view.findViewById(R.id.passwd_text);
        Button btnNext = view.findViewById(R.id.btnNext);
        title.setText(iniSection.get("security"));
        password.setText(iniSection.get("pwd_hint"));
        alert.setText(iniSection.get("toast_pwd_current"));
        btnNext.setText(iniSection.get("cancellation"));
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity2)getActivity()).replaceFragment(_Home.newInstance(),null);
            }
        });
        for(int i = 0; i < 6; i++){
            pw[i] = (EditText)view.findViewById(edit_id[i]);
            pw[0].requestFocus();
            final int finalI = i;
            pw[i].addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {}
                @Override
                public void afterTextChanged(Editable s) {
                    if(finalI < 5) {
                        pw[finalI + 1].requestFocus();
                   }else {
                        pwd = pw[0].getText().toString()+pw[1].getText().toString()+pw[2].getText().toString()+
                                pw[3].getText().toString()+pw[4].getText().toString()+pw[5].getText().toString();
                        pwdCheckRequest();
                    }
                }
            });
        }
        return view;
    }
    public void pwdCheckRequest(){
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("AppPwd", pwd);
        Call<String> call = getApiService(getActivity()).postPwdCheck(params);
        call.enqueue(new Callback<String>(){
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    Log.d("1","success");
                    ((MainActivity2)getActivity()).replaceFragment(SetPassWd.newInstance(),null);

                }
                else {
                    printToast(iniSection.get("toast_pwd_notmatch"));
                    for (int i = 0; i < 6; i++) {
                        pw[i].getText().clear();
                        pw[0].requestFocus();

                    }
                }
            }
            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t){
                Log.d("1","23");
                printToast(iniSection.get("toast_pwd_notmatch"));
            }
        });
    }
    public void printToast(String data){
        Toast.makeText(this.getActivity(), data, Toast.LENGTH_SHORT).show();
    }
}
