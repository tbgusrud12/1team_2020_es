package com.example.myongji_pay;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.myongji_pay.BottomMenu._Send;
import com.example.myongji_pay.ResponseData.TransferResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;
import static com.example.myongji_pay.RetrofitClient.getApiService;

public class Wire_Bank_Test extends Fragment {
    private View view;
    private boolean success =false;
    private TextView ress, reqq;
    private String bank;
    private String fintech_use_num;
    private EditText dps_print_content, wd_print_content, tran_amt, account;

    public static Wire_Bank_Test newInstance() {
        return new Wire_Bank_Test();
    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.wire_bank, container, false);
        Toolbar sub_toolbar = (Toolbar) view.findViewById(R.id.sub_toolbar);
        ((MainActivity2) getActivity()).setSupportActionBar(sub_toolbar);
        ((MainActivity2) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity2) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        TextView send = (TextView)view.findViewById(R.id.secText) ;
        send.setText(iniSection.get("title_Send"));



        Log.d("1",fintech_use_num);
        String[] str = getResources().getStringArray(R.array.spinnerArray);
        final ArrayAdapter adapter = new ArrayAdapter<String>(getContext(),R.layout.spinner_item, str);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        final Spinner spinner = (Spinner)view.findViewById(R.id.bank_spinner);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bank= (String)spinner.getSelectedItem();

                Log.i("i", "Spinner selected item = "+bank);

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        account = view.findViewById(R.id.account);
        dps_print_content = view.findViewById(R.id.dps_print_content);
        wd_print_content = view.findViewById(R.id.wd_print_content);
        tran_amt = view.findViewById(R.id.tran_amt);
        ress = view.findViewById(R.id.res);
        reqq = view.findViewById(R.id.req);
        Button next = (Button) view.findViewById(R.id.btnNext);

        account.setHint(iniSection.get("wire_account"));
        tran_amt.setHint(iniSection.get("wire_amount"));
        ress.setText(iniSection.get("wire_res_context"));
        reqq.setText(iniSection.get("wire_req_context"));
        next.setText(iniSection.get("check"));

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(account.getText().toString().replace(" ", "").equals("")){
                    printToast(iniSection.get("toast_error_account"));
                    account.requestFocus();
                    showKeypad();
                }
                else if(tran_amt.getText().toString().replace(" ", "").equals("")) {
                    printToast(iniSection.get("toast_error_tranfer"));
                    tran_amt.requestFocus();
                    showKeypad();
                }
                else if (dps_print_content.getText().toString().replace(" ", "").equals("")){
                    printToast(iniSection.get("toast_error_res_content"));
                    dps_print_content.requestFocus();
                    showKeypad();
                }
                else if (wd_print_content.getText().toString().replace(" ", "").equals("")){
                    printToast(iniSection.get("toast_error_req_content"));
                    wd_print_content.requestFocus();
                    showKeypad();
                }
                else {
                    postTransfer();
                }
            }

        });

        return view;

    }

    //서버에 입력된 정보로 회원가입 request
    public void postTransfer() {
        //params에 입력 데이터 삽입
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("fintech_use_num",fintech_use_num);
        params.put("dps_print_content", dps_print_content.getText().toString());
        params.put("wd_print_content", wd_print_content.getText().toString());
        params.put("tran_amt", tran_amt.getText().toString());
        params.put("recv_account", account.getText().toString());

        //POST 요청
        Call<TransferResponse> call = getApiService(getContext()).postTransfer(params);
        call.enqueue(new Callback<TransferResponse>(){
            @Override
            public void onResponse(Call<TransferResponse> call, Response<TransferResponse> response) {
                if(response.isSuccessful()){
                    success = true;
                    Log.d("postTransfer", "success POST");
                    Transfer.dps_account_num_masked = response.body().getDps_account_num_masked();
                    Transfer.dps_account_holder_name=response.body().getDps_account_holder_name();
                    Transfer.tran_amt = response.body().getTran_amt();
                    Transfer.api_tran_dtm = response.body().getApi_tran_dtm();
                    Transfer.api_tran_dtm= Transfer.api_tran_dtm.substring(0,8);
                    Intent intent = new Intent(getActivity(), FingerPrinter.class);
                    intent.putExtra("option", "wire");
                    startActivity(intent);
                }
                else{
                    success = false;
                    Log.e("postTransfer", "error code: " + response.code());
                    if(response.code() == 400)
                        printToast(iniSection.get("toast_not_entervalue"));

                    else if(response.code() == 500)
                        printToast(iniSection.get("toast_server_error"));
                    printToast(iniSection.get("error_to_error"));
                    FragmentManager fm = getParentFragmentManager();
                    fm.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    ((MainActivity2)getActivity()).replaceFragment(_Send.newInstance(),null);
                }

            }
            @Override
            public void onFailure(@NonNull Call<TransferResponse> call, @NonNull Throwable t){
                success = false;
                Log.e("postTransfer", "에러 : " + t.getMessage());
                printToast(iniSection.get("error_to_error"));
                FragmentManager fm = getParentFragmentManager();
                fm.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
                ((MainActivity2)getActivity()).replaceFragment(_Send.newInstance(),null);
            }
        });

    }
    public void printToast(String data){
        Toast.makeText(getContext(), data, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if(getArguments() != null) {
            Bundle edit = this.getArguments();
            fintech_use_num = edit.getString("fintech_use_num");
            Log.d("fintech_use_num", fintech_use_num);
        }
    }
    public void showKeypad(){
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }
}