package com.example.myongji_pay;

public class TransactionTimeItem extends TransactionDetailAdapterItem {

    public TransactionTimeItem(long time) {
        super(time);
    }

    public TransactionTimeItem(int year, int month, int dayOfMonth) {
        super(year, month, dayOfMonth);
    }

    @Override
    public int getType() {
        return TYPE_TIME;
    }
}
