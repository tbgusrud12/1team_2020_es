package com.example.myongji_pay;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import android.widget.TextView;
import android.widget.Toast;

public class TransactionDetailAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<TransactionDetailAdapterItem> itemList;
    private OnItemClickListener listener;

    public static class TimeViewHolder extends RecyclerView.ViewHolder {
        public TextView timeItemView;

        public TimeViewHolder(View v) {
            super(v);
            timeItemView = (TextView) v.findViewById(R.id.timeItemView);
        }
    }

    public static class DataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView timeView, titleView, amountView, p_timeView, bankView, typeView;

        private OnViewHolderClickListener listener;

        public DataViewHolder(View v, OnViewHolderClickListener listener) {
            super(v);
            //timeView = (TextView) v.findViewById(R.id.t_time);
            titleView = (TextView) v.findViewById(R.id.t_title);
            amountView = (TextView) v.findViewById(R.id.t_amount);
            p_timeView = (TextView) v.findViewById(R.id.t_time);
            bankView = (TextView) v.findViewById(R.id.t_bank);
            typeView = (TextView) v.findViewById(R.id.t_type);
            v.setOnClickListener(this);
            this.listener = listener;
        }

        @Override
        public void onClick(View v) {
            if(listener != null)
                listener.onViewHolderClick(getLayoutPosition());
        }

        private interface OnViewHolderClickListener {
            void onViewHolderClick(int position);
        }
    }

    public TransactionDetailAdpater(ArrayList<TransactionData> dataset) {
        itemList = initItemList(orderByTimeDesc(dataset));
    }

    private ArrayList<TransactionDetailAdapterItem> initItemList(ArrayList<TransactionData> dataset) {
        ArrayList<TransactionDetailAdapterItem> result = new ArrayList<>();

        int year = 0, month = 0, dayOfMonth = 0;
        for(TransactionData data:dataset) {
            if(year != data.getYear() || month != data.getMonth() || dayOfMonth != data.getDayOfMonth()) {
                result.add(new TransactionTimeItem(data.getYear(), data.getMonth(), data.getDayOfMonth()));
                year = data.getYear();
                month = data.getMonth();
                dayOfMonth = data.getDayOfMonth();
            }
            result.add(data);
            Log.d("title : ", String.valueOf(data.getTitle()));

        }
        return result;
    }

    private ArrayList<TransactionData> orderByTimeDesc(ArrayList<TransactionData> dataset) {
        ArrayList<TransactionData> result = dataset;
        for(int i=0; i<result.size()-1; i++) {
            for(int j=0; j<result.size()-i-1; j++) {
                if(result.get(j).getTime() < result.get(j+1).getTime()) {
                    TransactionData temp2 = result.remove(j+1);
                    TransactionData temp1 = result.remove(j);
                    result.add(j, temp2);
                    result.add(j+1, temp1);
                }
            }
        }
        return result;
    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position).getType();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == TransactionDetailAdapterItem.TYPE_TIME)
            return new TimeViewHolder(
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.transaction_detail_date, parent, false));
        else
            return new DataViewHolder(
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.transaction_detail_item, parent, false),
                    new DataViewHolder.OnViewHolderClickListener() {
                        @Override
                        public void onViewHolderClick(int position) {
                            if(listener != null)
                                listener.onItemClick(position);

                        }
                    }
            );
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof TimeViewHolder) {
            TimeViewHolder tHolder = (TimeViewHolder) holder;
            tHolder.timeItemView.setText(itemList.get(position).getTimeToString());
            Log.d("날짜 : ", String.valueOf(( itemList.get(position)).getTimeToString()));

        } else {
            DataViewHolder dHolder = (DataViewHolder) holder;
            //dHolder.timeView.setText(itemList.get(position).getTimeToString());
            dHolder.titleView.setText(((TransactionData)itemList.get(position)).getTitle());
            dHolder.amountView.setText(((TransactionData)itemList.get(position)).getAmount());
            dHolder.p_timeView.setText(((TransactionData)itemList.get(position)).getPTime());
            dHolder.bankView.setText(((TransactionData)itemList.get(position)).getBank());
            dHolder.typeView.setText(((TransactionData)itemList.get(position)).getType_());
            Log.d("내역 : ", (String)dHolder.titleView.getText());
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public TransactionData getItem(int position) {
        return (TransactionData)itemList.get(position);
    }

    public interface OnItemClickListener {
        public void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}