package com.example.myongji_pay;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import static com.example.myongji_pay.DrawerMenu.Drawer_Language.iniSection;

public class WireEnd extends AppCompatActivity {
    TextView recv_client_name, cntr_account_num, tran_amt, tran_dtime, res, success, date, am;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wire_end);
        recv_client_name = findViewById(R.id.recv_client_name);
        cntr_account_num = findViewById(R.id.cntr_account_num);
        tran_amt = findViewById(R.id.tran_amt);
        tran_dtime = findViewById(R.id.tran_dtime);
        am = findViewById(R.id.am);
        res = findViewById(R.id.res);
        success = findViewById(R.id.success);
        date=findViewById(R.id.date);
        am.setText(iniSection.get("send_amount"));
        res.setText(iniSection.get("incoming_account"));
        success.setText(iniSection.get("remittance_complete"));
        date.setText(iniSection.get("remittance_date"));
        recv_client_name.setText(Transfer.dps_account_holder_name);
        cntr_account_num.setText(Transfer.dps_account_num_masked);
        tran_amt.setText(Transfer.tran_amt + iniSection.get("won"));
        tran_dtime.setText(Transfer.api_tran_dtm);

        Button btnNext = (Button) findViewById(R.id.next);
        btnNext.setText(iniSection.get("check"));
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),MainActivity2.class);
                startActivity(intent);
            }
        });

    }
}

